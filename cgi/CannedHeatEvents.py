#!/usr/bin/python
#
# Copyright (C) 2014 Juha Leino
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without
# limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import gobject
import dbus
from dbus.mainloop.glib import DBusGMainLoop
import os
import sys
import struct
import time

def dbus_measurer_signal_handler(timestamp, value1, value2):
    t = int(time.mktime(time.strptime(timestamp, "%Y-%m-%d %H:%M:%S")))
    try:
        print 'data: T,%i,%f,%f\r\n' % (t, value1, value2)
        sys.stdout.flush()
    except IOError as e:
        sys.stderr.write("I/O error({0}): {1}".format(e.errno, e.strerror))
        sys.exit(1)

def dbus_vent_signal_handler(timestamp, value1, movement_finished):
    t = int(time.mktime(time.strptime(timestamp, "%Y-%m-%d %H:%M:%S")))
    try:
        print 'data: V,%i,%f,%i\r\n' % (t, value1, movement_finished)
        sys.stdout.flush()
    except IOError as e:
        sys.stderr.write("I/O error({0}): {1}".format(e.errno, e.strerror))
        sys.exit(1)

def dbus_controller_signal_handler(timestamp, is_enabled, grill_target_temperature,
    meat_target_temperature):
    t = int(time.mktime(time.strptime(timestamp, "%Y-%m-%d %H:%M:%S")))
    try:
        print 'data: C,%i,%i,%f,%f\r\n' % (t, is_enabled, grill_target_temperature,
            meat_target_temperature)
        sys.stdout.flush()
    except IOError as e:
        sys.stderr.write("I/O error({0}): {1}".format(e.errno, e.strerror))
        sys.exit(1)


print 'Content-Type: text/event-stream'
print 'Cache-Control: no-cache\r\n'

loop = DBusGMainLoop(set_as_default=True)
system_bus = dbus.SystemBus(mainloop=loop)
system_bus.add_signal_receiver(dbus_measurer_signal_handler, 'TemperatureMeasurement', 
    'fi.juhakleino.cannedheat.measurer')
system_bus.add_signal_receiver(dbus_vent_signal_handler, 'stateChanged', 
    'fi.juhakleino.cannedheat.vent')
system_bus.add_signal_receiver(dbus_controller_signal_handler, 'configurationChanged',
    'fi.juhakleino.cannedheat.temperature_controller')

gobject.MainLoop().run()

