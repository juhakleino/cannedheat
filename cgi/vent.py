#!/usr/bin/python
#
# Copyright (C) 2014 Juha Leino
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without
# limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import dbus
import sys
import json

def get_vent_state(vent):
    state = vent.Get('fi.juhakleino.cannedheat.vent', 'state',
        dbus_interface='org.freedesktop.DBus.Properties')
    return state
    
def set_vent_state(vent, new_state):
    vent.Set('fi.juhakleino.cannedheat.vent', 'state', new_state,
        dbus_interface='org.freedesktop.DBus.Properties')

def generate_http_get_reply(state):
    print "Content-type: text/json"
    print
    print '{ "value": ' + str(state) + ' }'

def main():
    bus = dbus.SystemBus()
    vent = bus.get_object('fi.juhakleino.cannedheat.vent',
        '/fi/juhakleino/cannedheat/vent/object')
    method = os.environ['REQUEST_METHOD']
    if method == 'GET':
        vent_state = get_vent_state(vent)
        generate_http_get_reply(vent_state)
    elif method == 'PUT':
        input_data = sys.stdin.read()
        data = json.loads(input_data)
        set_vent_state(vent, data[u'value'])        
        print "Status: 200 Ok\n"

if __name__ == "__main__":
    main()

