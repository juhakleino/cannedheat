#!/usr/bin/python
#
# Copyright (C) 2014 Juha Leino
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without
# limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import struct
import time
import os
import sys
from bsddb3 import db
import dbus

MEASUREMENT_DB_FILE_NAME = '/var/cannedheat/temperature_measurements'
CONSOLIDATION_WINDOW_LENGTH = 60
CONSOLIDATION_THRESHOLD = 30*60
DO_NOT_CARE_VALUE = -100

initial_consolidation_data = {'window_start': None, 'window_end': None, 
    'grill': 0, 'meat': 0, 'grill_counter': 0, 'meat_counter': 0,  
    'vent': DO_NOT_CARE_VALUE, 'vent_first': DO_NOT_CARE_VALUE,  
    'grill_target': DO_NOT_CARE_VALUE, 'grill_target_first': DO_NOT_CARE_VALUE, 
    'finished': True}

def update_consolidation_data(consolidation_data, timestamp, grill_temperature, meat_temperature, vent_state, grill_target):
    """ Collects data from the events (rows) that are to be consolidated as one or two events. Temperature fields
    are averaged to one event. In the case of the vent state and grill target temperature, both the first and final value
    of the variable are stored to indicate when their values are changed. 
    """
    if consolidation_data['window_start'] is None:
        consolidation_data['window_start'] = timestamp
        consolidation_data['finished'] = False
    if consolidation_data['window_start'] < timestamp-CONSOLIDATION_WINDOW_LENGTH:
        consolidation_data['finished'] = True
        return  # The end of the consolidation time window is passed
    consolidation_data['window_end'] = timestamp
        
    if grill_temperature != DO_NOT_CARE_VALUE:
        consolidation_data['grill'] += grill_temperature
        consolidation_data['grill_counter'] += 1
    if meat_temperature != DO_NOT_CARE_VALUE:
        consolidation_data['meat'] += meat_temperature
        consolidation_data['meat_counter'] += 1
    if vent_state != DO_NOT_CARE_VALUE:
        consolidation_data['vent'] = vent_state
        if consolidation_data['vent_first'] == DO_NOT_CARE_VALUE:
            consolidation_data['vent_first'] = vent_state
    if grill_target != DO_NOT_CARE_VALUE:
        consolidation_data['grill_target'] = grill_target
        if consolidation_data['grill_target_first'] == DO_NOT_CARE_VALUE:
            consolidation_data['grill_target_first'] = grill_target
    
def print_consolidated_data(consolidation_data):
    """ Prints the consolidated events based on the data collected by update_consolidation_data()
    function. If initial and final values of the vent state or grill target temperature differ,
    two separate events are printed the first one of which contains only the initial values
    of these variables. This is to show the stepwise changes of the variables in the UI.
    The other event contains averaged values of the temperature variables and final values of
    the aforementioned variables.
    """
    consolidated_timestamp = (consolidation_data['window_start']+consolidation_data['window_end'])/2
    if consolidation_data['grill_counter'] > 0:
        consolidated_grill_temperature = consolidation_data['grill']/consolidation_data['grill_counter']
    else:
        consolidated_grill_temperature = DO_NOT_CARE_VALUE
    if consolidation_data['meat_counter'] > 0:
        consolidated_meat_temperature = consolidation_data['meat']/consolidation_data['meat_counter']
    else:
        consolidated_meat_temperature = DO_NOT_CARE_VALUE
        
    if (consolidation_data['vent_first'] != consolidation_data['vent'] 
        or consolidation_data['grill_target_first'] != consolidation_data['grill_target']):
        sys.stdout.write('[%i,%f,%f,%f,%f],\n' % (consolidated_timestamp, 
            DO_NOT_CARE_VALUE, DO_NOT_CARE_VALUE, consolidation_data['vent_first'],
            consolidation_data['grill_target_first']))
    sys.stdout.write('[%i,%f,%f,%f,%f]' % (consolidated_timestamp, 
        consolidated_grill_temperature, consolidated_meat_temperature, consolidation_data['vent'],
        consolidation_data['grill_target']))

def db_contents():
    """ Prints out the event data from the database formatted as JSON arrays. The events
    old enough are consolidated as summary events to reduce their number.
    """
    # Variables for consolidation algorithm
    consolidation_time_limit = time.time()-CONSOLIDATION_THRESHOLD
    consolidation_data = initial_consolidation_data.copy()

    measurement_db = db.DB()
    
    try:
        measurement_db.open(MEASUREMENT_DB_FILE_NAME, None, db.DB_UNKNOWN, db.DB_RDONLY)
    except db.DBNoSuchFileError:
        return # OK, nothing to do
        
    cursor = measurement_db.cursor()
    rec = cursor.first()
    while rec:
        timestring = rec[0]
        # Unpack the C struct data stored to DB
        grill_temperature,meat_temperature,vent_state, controller_enabled, grill_target,meat_target = struct.unpack('@dddddd', rec[1])
        timestamp = int(time.mktime(time.strptime(timestring, "%Y-%m-%d %H:%M:%S\0")))

        if consolidation_data['finished']:
            # No consolidation ongoing
            if timestamp > consolidation_time_limit:
                # The new enough events are printed as such
                sys.stdout.write('[%i,%f,%f,%f,%f]' % (timestamp, grill_temperature, meat_temperature, vent_state, grill_target))
            else:
                # Start a new consolidation (and do not print anything yet)
                consolidation_data = initial_consolidation_data.copy()
                update_consolidation_data(consolidation_data, timestamp, grill_temperature, meat_temperature, vent_state, grill_target)
        else:
            # Update consolidation data
            update_consolidation_data(consolidation_data, timestamp, grill_temperature, meat_temperature, vent_state, grill_target)
            if consolidation_data['finished']:
                # End of the consolidation time window reached. Print the consolidated event(s) and
                # move the cursor one step backwards because the current event was not included to
                # the consolidated data
                print_consolidated_data(consolidation_data)
                cursor.prev()

        rec = cursor.next()
        if rec and consolidation_data['finished']:
            sys.stdout.write(',')
            sys.stdout.write('\n')
    
    if not consolidation_data['finished']:
        # If the last consolidation was not finished, process it now
        print_consolidated_data(consolidation_data)
        sys.stdout.write('\n')
        
    measurement_db.close()
        
def delete_measurements():
    """ Sends a command to clean up the DB over DBus
    """
    bus = dbus.SystemBus()
    logger = bus.get_object('fi.juhakleino.cannedheat.logger',
        '/fi/juhakleino/cannedheat/logger/object')
    logger.Clear(dbus_interface='fi.juhakleino.cannedheat.logger')

def main():
    method = os.environ['REQUEST_METHOD']
    if method == 'GET':
        sys.stdout.write("Content-type:application/json\n\n")
        sys.stdout.write('[')
        db_contents()
        sys.stdout.write(']\n')
    if method == 'DELETE':
        delete_measurements()
        sys.stdout.write("Status: 200 Ok\n\n")
        
if __name__ == "__main__":
    main()
