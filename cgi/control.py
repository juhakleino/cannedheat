#!/usr/bin/python
#
# Copyright (C) 2014 Juha Leino
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without
# limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import dbus
import sys
import json

def get_controller_data(controller):
    controllerEnabled = controller.Get('fi.juhakleino.cannedheat.temperature_controller',
        'enabled', dbus_interface='org.freedesktop.DBus.Properties')
    grillTargetTemperature = controller.Get('fi.juhakleino.cannedheat.temperature_controller',
        'grillTargetTemperature', dbus_interface='org.freedesktop.DBus.Properties')
    meatTargetTemperature = controller.Get('fi.juhakleino.cannedheat.temperature_controller',
        'meatTargetTemperature', dbus_interface='org.freedesktop.DBus.Properties')
    return { 
        'manual': not controllerEnabled, 
        'grillTargetTemperature': grillTargetTemperature,
        'meatTargetTemperature': meatTargetTemperature
    }
    
def set_controller_data(controller, new_data):
    controller.Set('fi.juhakleino.cannedheat.temperature_controller', 'enabled', 
        (not new_data[u'manual']), dbus_interface='org.freedesktop.DBus.Properties')
    controller.Set('fi.juhakleino.cannedheat.temperature_controller', 'grillTargetTemperature',
        new_data[u'grillTargetTemperature'], dbus_interface='org.freedesktop.DBus.Properties')
    controller.Set('fi.juhakleino.cannedheat.temperature_controller', 'meatTargetTemperature',
        new_data[u'meatTargetTemperature'], dbus_interface='org.freedesktop.DBus.Properties')

def generate_http_get_reply(data):
    sys.stdout.write("Content-type: text/json\n\n")
    sys.stdout.write('{ "manual": ' + str(data['manual']).lower() + ',\n')
    sys.stdout.write('"grillTargetTemperature": ' + str(data['grillTargetTemperature']) + ',\n')
    sys.stdout.write('"meatTargetTemperature": ' + str(data['meatTargetTemperature']) + '}\n')

def main():
    bus = dbus.SystemBus()
    controller = bus.get_object('fi.juhakleino.cannedheat.temperature_controller',
        '/fi/juhakleino/cannedheat/temperature_controller/object')
    method = os.environ['REQUEST_METHOD']
    if method == 'GET':
        controller_data = get_controller_data(controller)
        generate_http_get_reply(controller_data)
    elif method == 'PUT':
        input_data = sys.stdin.read()
        data = json.loads(input_data)
        set_controller_data(controller, data)        
        print "Status: 200 Ok\n"

if __name__ == "__main__":
    main()

