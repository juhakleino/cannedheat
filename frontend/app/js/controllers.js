'use strict';

// Finnish language used as an easy to way to fix the date format in the Google charts
google.load('visualization', '1.0', {'packages':['corechart', 'gauge'], 'language': 'fi'});
google.setOnLoadCallback(function() {
    angular.bootstrap(document.body, ['chApp']);
});

// Operation view controller
angular.module('chApp.controllers', [])
    .controller('OperationCtrl', ['$scope', '$timeout', '$q', 'MeasurementSource',
            'StoredMeasurements', 'ChartOptions', 'State', 'Vent', 'Control', 'Constants',
        function($scope, $timeout, $q, MeasurementSource, StoredMeasurements, ChartOptions, 
                State, Vent, Control, Constants) {
            // Data for Google Charts components
            $scope.grillGaugeData = { 
              data: google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['Grill', 0]]),
              options: ChartOptions.grillGauge
            };
            $scope.meatGaugeData = { 
              data: google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['Meat', 0]]),
              options: ChartOptions.meatGauge
            };
            $scope.lineChartData = {
                data: new google.visualization.DataTable(),
                options: ChartOptions.lineChart
            };
            
            // Line chart data model
            $scope.lineChartData.data.addColumn('datetime', 'Time');
            $scope.lineChartData.data.addColumn('number', 'Grill');
            $scope.lineChartData.data.addColumn('number', 'Meat');
            $scope.lineChartData.data.addColumn('number', 'Vent');
            $scope.lineChartData.data.addColumn('number', 'Grill Target');
            $scope.lineChartDataDirtyFlag = true;
            
            // Line chart time (horizontal) window (0: 'whole data set') 
            $scope.timeWindow = 30;
            
            // State variables
            var latestReceivedVentState = null;
            var latestReceivedGrillTargetTemperature = null;
            var latestGrillTemperature = null;
            var latestMeatTemperature = null;
            var consolidationPeriodStartTime = null;
            var consolidationPeriodStartIndex = null;
            var consolidationThreshold = 1800000; // 30 min
            var consolidationWindow = 60000; // 60 s

            // Round the valid numbers and convert invalid or 'do-not-care'
            // values to null objects
            function formatMeasurementValue(measurement_value) {
                if (typeof(measurement_value) != 'number' 
                    || Constants.missingMeasurementValue == measurement_value) {
                    return null;
                } else {
                    return Math.round(10.0*measurement_value)/10.0;
                }
            }
            
            // The consolidation mechanims to reduce the number of older events.
            // The algorithm is same as implemented in measurements.py script which
            // consolidates the events found from the DB. This function is used
            // for events received online (not from the DB) and which has been 
            // kept in memory long enough to summarize them. 
            function consolidateChartData(dataTable, startIndex) {
                var lastTimestamp = null;
                var grillTemperatureSum = 0.0;
                var grillMeasurementCounter = 0;
                var meatTemperatureSum = 0.0;
                var meatMeasurementCounter = 0;
                var lastVentState = null;
                var lastGrillTarget = null;
                var firstVentState = null;
                var firstGrillTarget = null;
                
                var i = startIndex;
                var rowCounter = 0;
                var rowOffset = 0;
                var tmp = null;
                var limit = (dataTable.getValue(startIndex, 0)).getTime()+consolidationWindow;

                // Collect data from the events inside the consolidation time window                
                while (i < dataTable.getNumberOfRows()
                        && limit >= (dataTable.getValue(i, 0)).getTime()) {
                    rowCounter++;
                    lastTimestamp = dataTable.getValue(i, 0);
                    
                    tmp = dataTable.getValue(i, 1);
                    if (typeof(tmp) == 'number') {
                       grillMeasurementCounter++;
                       grillTemperatureSum += tmp;
                    }
                    
                    tmp = dataTable.getValue(i, 2);
                    if (typeof(tmp) == 'number') {
                       meatMeasurementCounter++;
                       meatTemperatureSum += tmp;
                    }
                    
                    tmp = dataTable.getValue(i, 3);
                    if (typeof(tmp) == 'number') {
                        lastVentState = tmp;
                        if (firstVentState == null) {
                            firstVentState = tmp;
                        }
                    }
                    
                    tmp = dataTable.getValue(i, 4);
                    if (typeof(tmp) == 'number') {
                        lastGrillTarget = tmp;
                        if (firstGrillTarget == null) {
                            firstGrillTarget = tmp;   
                        }
                    }
                    
                    i++;
                }
                
                if (rowCounter > 1) {
                    var averageTime = (dataTable.getValue(startIndex, 0)).getTime()/2;
                    averageTime += lastTimestamp.getTime()/2;
                  
                    // If the vent state or the grill target temperature has been changed
                    // during the consolidation window, create an extra row to the data model
                    if (firstVentState != lastVentState
                        || firstGrillTarget != lastGrillTarget) {
                        dataTable.setValue(startIndex, 0, new Date(Math.round(averageTime)));
                        dataTable.setValue(startIndex, 1, null);
                        dataTable.setValue(startIndex, 2, null);
                        dataTable.setValue(startIndex, 3, firstVentState);
                        dataTable.setValue(startIndex, 4, firstGrillTarget);
                        rowOffset++;
                    }
                    
                    // Store the consolidated data for the line chart
                    dataTable.setValue(startIndex+rowOffset, 0, new Date(Math.round(averageTime)));
                    dataTable.setValue(startIndex+rowOffset, 1, grillTemperatureSum/grillMeasurementCounter);
                    dataTable.setValue(startIndex+rowOffset, 2, meatTemperatureSum/meatMeasurementCounter);
                    dataTable.setValue(startIndex+rowOffset, 3, lastVentState);
                    dataTable.setValue(startIndex+rowOffset, 4, lastGrillTarget);
                    
                    // Remove consolidated rows from the data model
                    dataTable.removeRows(startIndex+rowOffset+1, rowCounter-1-rowOffset);
                }                
                return rowOffset+1; // Indicate how many rows were left to the data model 
            }
            
            // Handle the temperature event. Updates internal state variables, gauge
            // data models, starts the temperature alarm checking and creates an array
            // to be inserted to the line chart data model
            function ProcessTemperatureEvent(eventObject, rowArray) {
                latestGrillTemperature = eventObject.grillTemperature;
                latestMeatTemperature = eventObject.meatTemperature;
                $scope.grillGaugeData.data.setCell(0, 1, Math.round(latestGrillTemperature));  
                $scope.meatGaugeData.data.setCell(0, 1, Math.round(latestMeatTemperature));
                rowArray.push(eventObject.timestamp); 
                rowArray.push(formatMeasurementValue(eventObject.grillTemperature));
                rowArray.push(formatMeasurementValue(eventObject.meatTemperature));
                rowArray.push(latestReceivedVentState);
                rowArray.push(latestReceivedGrillTargetTemperature);
                startTemperatureAlarmsChecking();
            }

            // Handle the vent state event. Updates the data models of the vent slider
            // and creates an array to be inserted to the line chart data model
            function ProcessVentStateEvent(eventObject, rowArray) {
                latestReceivedVentState = formatMeasurementValue(eventObject.ventState);
                if (eventObject.movementFinished) {
                    $scope.ventState.value = eventObject.ventState;
                }
                rowArray.push(eventObject.timestamp); 
                rowArray.push(null);
                rowArray.push(null);
                rowArray.push(formatMeasurementValue(latestReceivedVentState));
                rowArray.push(latestReceivedGrillTargetTemperature);
            }
            
            // Handle the temperature controller configuration event (controller on/off and
            // the target temperatures). Updates the data models used by the UI inputs and
            // creates an array to be inserted to the line chart data model
            function ProcessControllerConfigurationEvent(eventObject, rowArray) {
                latestReceivedGrillTargetTemperature = formatMeasurementValue(eventObject.grillTargetTemperature);
                if (eventObject.enabled == $scope.ventControl.manual) {
                    $scope.ventControl.manual = !$scope.ventControl.manual;
                }
                if (eventObject.grillTargetTemperature != $scope.ventControl.grillTargetTemperature) {
                    $scope.ventControl.grillTargetTemperature = eventObject.grillTargetTemperature;
                }
                if (eventObject.meatTargetTemperature != $scope.ventControl.meatTargetTemperature) {
                    $scope.ventControl.meatTargetTemperature = eventObject.meatTargetTemperature;
                }
                rowArray.push(eventObject.timestamp); 
                rowArray.push(null);
                rowArray.push(null);
                rowArray.push(null);
                rowArray.push(latestReceivedGrillTargetTemperature);
            }
            
            // 'Subscribe' the measurement events and handle them in the notify callback
            MeasurementSource.onlineMeasurements().then(null, null, 
                function(eventObject) {
                    var rowArray = [];

                    if (eventObject.type == Constants.temperatureEventType) {
                        ProcessTemperatureEvent(eventObject, rowArray);
                    } else if (eventObject.type == Constants.ventEventType) {
                        ProcessVentStateEvent(eventObject, rowArray);
                    } else if (eventObject.type == Constants.controllerEventType) {
                        ProcessControllerConfigurationEvent(eventObject, rowArray);
                    }
                    
                    if (typeof($scope.running.value) == 'boolean' 
                        && $scope.running.value == true) {
                        // Insert a row to the line chart data model
                        $scope.lineChartData.data.addRow(rowArray);
                        $scope.lineChartDataDirtyFlag = !$scope.lineChartDataDirtyFlag;

                        // Control the consolidation algorithm
                        if (consolidationPeriodStartIndex == null) {
                            // If the consolidation window reference variables were not initialized
                            // when the measurements were read from DB, do it now
                            consolidationPeriodStartIndex = $scope.lineChartData.data.getNumberOfRows()-1;
                            consolidationPeriodStartTime = eventObject.timestamp.getTime();
                        } else if (eventObject.timestamp.getTime() > 
                                consolidationPeriodStartTime+consolidationThreshold+consolidationWindow) {
                            // The consolidation window is old enough to process it
                            var lines;
                            lines = consolidateChartData($scope.lineChartData.data, consolidationPeriodStartIndex);                            
                            consolidationPeriodStartIndex += lines;
                            consolidationPeriodStartTime = 
                                $scope.lineChartData.data.getValue(consolidationPeriodStartIndex, 0).getTime();
                        }
                    }
                }
            );

            // Load measurements available when the controller is created
            var storedMeasurements = StoredMeasurements.query().$promise.then(function(measurements) {
                var time_limit = ((new Date()).getTime()-consolidationThreshold)/1000;
                consolidationPeriodStartIndex = null;
                consolidationPeriodStartTime = null;
                var measurement;

                // Clear the current chart data
                $scope.lineChartData.data.removeRows(0, $scope.lineChartData.data.getNumberOfRows());

                // Loop over the events from the DB                
                for (measurement = 0; measurement < measurements.length; measurement++) {
                    var tmp = measurements[measurement];
                    
                    if (measurements[measurement][0] >= time_limit
                        && consolidationPeriodStartIndex == null) {
                        // Initialize the consolidation window reference variables
                        consolidationPeriodStartIndex = parseInt(measurement);
                        consolidationPeriodStartTime = measurements[measurement][0]*1000;
                    }
                    // Add row to the line chart data
                    var row = [new Date(measurements[measurement][0] * 1000), 
                        formatMeasurementValue(measurements[measurement][1]), 
                        formatMeasurementValue(measurements[measurement][2]),
                        formatMeasurementValue(measurements[measurement][3]),
                        formatMeasurementValue(measurements[measurement][4])
                    ];
                    $scope.lineChartData.data.addRow(row);    
                }
                $scope.lineChartDataDirtyFlag = !$scope.lineChartDataDirtyFlag;
            });
            
            // Fetch the 'running' state from the backend
            $scope.running = State.get();

            // Forward the 'running' state changes to the backend
            $scope.$watch("running.value", function(newValue, oldValue) {
                if (newValue != oldValue && typeof(oldValue) != 'undefined') {
                    State.set($scope.running);
                }
            });
            
            // Handling of Clear button. Deletes all measurement data both from the client
            // and the backend. The measurement collection is stopped temporarily
            // to simplify the initialization of the cleared DB.
            $scope.clear = function() {
                var r=confirm("The current cooking session data will be deleted!");
                if (r==true) {
                    
                    var initial_running_state = $scope.running.value;
                    var running_state = {value: false};

                    State.set(running_state, function() {
                        var storedMeasurements = StoredMeasurements.delete(function() {
                            $scope.lineChartData.data.removeRows(0, 
                                $scope.lineChartData.data.getNumberOfRows());
                            $scope.lineChartDataDirtyFlag = !$scope.lineChartDataDirtyFlag;
                            if (initial_running_state) {
                                running_state.value = true;
                                State.set(running_state);
                            }
                        });
                    });
                }
            };
            
            // Vent state handling
            $scope.ventState = Vent.get(function(ventState) {
                latestReceivedVentState = ventState.value;
            });
            
            $scope.handleVentChange = function() {
                Vent.set($scope.ventState);
            }
            
            $scope.handleVentSliderClicked = function() {
                Vent.set($scope.ventState);
            }
            
            // Temperature alarm handling and variables
            $scope.grillTemperatureAlarmLimit = Constants.defaultGrillTemperatureAlarmLimit;
            $scope.alarmControl = {
                grill: false,
                meat: false
            };
            var alarmTimeout = null;
            
            // Updates the yellow and red areas of the gauge charts. The yellow area is set to 
            // from the actual limit to 10% over the limit.
            function updateGaugeOptions() {
                $scope.grillGaugeData.options.yellowFrom = $scope.ventControl.grillTargetTemperature;
                $scope.grillGaugeData.options.yellowTo = Math.round(1.1 * $scope.ventControl.grillTargetTemperature);
                $scope.grillGaugeData.options.redFrom = Math.round(1.1 * $scope.ventControl.grillTargetTemperature);
                $scope.meatGaugeData.options.yellowFrom = $scope.ventControl.meatTargetTemperature;
                $scope.meatGaugeData.options.yellowTo = Math.round(1.1 * $scope.ventControl.meatTargetTemperature);
                $scope.meatGaugeData.options.redFrom = Math.round(1.1 * $scope.ventControl.meatTargetTemperature);
            }
            
            // Initialize the controller configuration parameters
            $scope.ventControl = Control.get(function() {
                latestReceivedGrillTargetTemperature = $scope.ventControl.grillTargetTemperature;
                updateGaugeOptions();
            });
            
            // Update the controller configuration parameters
            $scope.controllerParametersChanged = function() {
                Control.set($scope.ventControl);
                updateGaugeOptions();
            };
            
            $scope.alarmStateChanged = function() {
                startTemperatureAlarmsChecking();
            };
            
            // Play the alarm sound
            function PlayAlarm() {
                var testAudio = new Audio();
                var alarmAudio = null;
                if (testAudio.canPlayType("audio/ogg; codecs=vorbis")) {
                    alarmAudio = new Audio("audio/beep.ogg");
                } else if (testAudio.canPlayType("audio/mp3: codecs=mp3")) {
                    alarmAudio = new Audio("audio/beep.mp3");   
                }
                
                if (alarmAudio) {
                    alarmAudio.play();   
                } else {
                    alert("Cannot play alarm sound");   
                }
            }
            
            // Initialize the regular alarm checking if it's not running yet 
            function startTemperatureAlarmsChecking() {
                if (!alarmTimeout) {
                    checkTemperatureAlarms();
                }
            }
            
            // The alarm checking function. If the temperatures variables are within the limits,
            // just nullify alarmTimeout variable. Otherwise, play the alarm and initialize
            // the timeout calls the function again later.
            function checkTemperatureAlarms() {
                if (($scope.alarmControl.grill 
                    && typeof(latestGrillTemperature) == 'number' 
                    && $scope.grillTemperatureAlarmLimit <= 
                        Math.abs(latestGrillTemperature-$scope.ventControl.grillTargetTemperature))
                    || ($scope.alarmControl.meat
                        && typeof(latestMeatTemperature) == 'number' 
                        && latestMeatTemperature >= $scope.ventControl.meatTargetTemperature)) {
                    PlayAlarm();
                    alarmTimeout = $timeout(checkTemperatureAlarms, Constants.alarmFrequency, false);
                } else {
                    alarmTimeout = null;
                }
            }
        }
    ])

