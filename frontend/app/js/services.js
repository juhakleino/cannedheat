'use strict';

var serviceModule = angular.module('chApp.services', []);

// Service for receiving (server sent) events from the backend
serviceModule.factory('MeasurementSource', ['$q', 'Constants',
    function($q, Constants) { return {
        onlineMeasurements: function() {
            var deferred = $q.defer();
            
            var source=new EventSource("/cgi-bin/CannedHeatEvents.py");
            source.onmessage = function(event) {
                var eventFields = event.data.split(",");
                var eventObject = {};
                var valid = true;
                
                var eventTypeTag = eventFields[0];
                
                if (eventTypeTag == "T") {
                    eventObject.type = Constants.temperatureEventType;
                    
                    var field = parseInt(eventFields[1]);
                    if (typeof(field) == 'number') {
                        eventObject.timestamp = new Date(field*1000);
                    } else {
                        valid = false;
                    }
                    
                    field = parseFloat(eventFields[2]);
                    if (typeof(field) == 'number') {
                        eventObject.grillTemperature = field;
                    } else {
                        eventObject.grillTemperature = null;
                    }
                    
                    field = parseFloat(eventFields[3]);
                    if (typeof(field) == 'number') {
                        eventObject.meatTemperature = field;
                    } else {
                        eventObject.meatTemperature = null;
                    }
                } else if (eventTypeTag == "V") {
                    eventObject.type = Constants.ventEventType;
                    
                    var field = parseInt(eventFields[1]);
                    if (typeof(field) == 'number') {
                        eventObject.timestamp = new Date(field*1000);
                    } else {
                        valid = false;
                    }
                    
                    field = parseFloat(eventFields[2]);
                    if (typeof(field) == 'number') {
                        eventObject.ventState = field;
                    } else {
                        valid = false;
                    }

                    field = parseInt(eventFields[3]);
                    if (typeof(field) == 'number') {
                        eventObject.movementFinished = (field != 0) ? true : false;
                    } else {
                        valid = false;
                    }
                } else if (eventTypeTag == "C") {
                    eventObject.type = Constants.controllerEventType;
                    var field = parseInt(eventFields[1]);
                    if (typeof(field) == 'number') {
                        eventObject.timestamp = new Date(field*1000);
                    } else {
                        valid = false;
                    }
                    
                    field = parseInt(eventFields[2]);
                    if (typeof(field) == 'number') {
                        eventObject.enabled = (field == 0) ? false : true;
                    } else {
                        valid = false;
                    }
                    
                    field = parseFloat(eventFields[3]);
                    if (typeof(field) == 'number') {
                        eventObject.grillTargetTemperature = field;
                    } else {
                        valid = false;
                    }
                    
                    field = parseFloat(eventFields[4]);
                    if (typeof(field) == 'number') {
                        eventObject.meatTargetTemperature = field;
                    } else {
                        valid = false;
                    }
                }

                if (valid) {
                    deferred.notify(eventObject);
                }
            };

            return deferred.promise;
        }
    }}
]);

// RESTful service for reading and deleting measurements (as a collection)
serviceModule.factory('StoredMeasurements', ['$resource',  
    function($resource) { 
        return $resource('resource/measurements', {}, {
            query: { method:'GET', isArray:true },
        });
    }
]);

// RESTful service for managing the 'running' state of the system
serviceModule.factory('State', ['$resource',  
    function($resource) { 
        return $resource('resource/state', {}, {
            get: { method:'GET', isArray:false },
            set: { method:'PUT' }
        });
    }
]);

// RESTful service for managing the air vent
serviceModule.factory('Vent', ['$resource',  
    function($resource) { 
        return $resource('resource/vent', {}, {
            get: { method:'GET', isArray:false },
            set: { method:'PUT' }
        });
    }
]);

// RESTful service for managing the automatic temperature controller service
serviceModule.factory('Control', ['$resource',  
    function($resource) { 
        return $resource('resource/control', {}, {
            get: { method:'GET', isArray:false },
            set: { method:'PUT' }
        });
    }
]);


