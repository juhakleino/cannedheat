'use strict';

/* Directives */

angular.module('chApp.directives', [])
    // Gauge chart directive
    .directive('gaugeChart', function () {
        return {
            restrict: 'E',
            scope: {
                chartData : '=data'
            },
            link: function ($scope, elm, attrs) {
                var chart = " ";
                chart = new google.visualization.Gauge(elm[0]);
        
                $scope.$watch('chartData', function () {
                    chart.draw($scope.chartData.data, $scope.chartData.options);
                }, true);
                
                chart.draw($scope.chartData.data, $scope.chartData.options);
            }
        }
    })
    // Line chart directive
    .directive('lineChart', function () {
        return {
            restrict: 'E',
            scope: {
                chartData: '=data',
                chartDataDirtyFlag: '=dirtyFlag',
                timeWindow: '='
            },
            link: function ($scope, elm, attrs) {
                var chart = " ";
                //chart = new google.visualization.LineChart(elm[0]);
                chart = new google.visualization.ComboChart(elm[0]);
        
                $scope.$watch(
                    // Watcher function. The dirty flag is used for the chart data
                    // array because the array itself may grow quite big. 
                    function() {
                        return [$scope.chartDataDirtyFlag, $scope.timeWindow];
                    },
                    // Redrawing the line chart
                    function () {
                        var now = new Date();
                        
                        if ($scope.timeWindow > 0) {
                            $scope.chartData.options.hAxis = { viewWindow: { 
                                min: new Date(now.getTime()-60000*$scope.timeWindow),
                                max: now
                            }};
                        } else {
                            $scope.chartData.options.hAxis = null;
                        }
                        chart.draw($scope.chartData.data, $scope.chartData.options);
                    }
                , true);
                
                chart.draw($scope.chartData.data, $scope.chartData.options);
            }
        }
    });
