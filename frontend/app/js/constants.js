
var constantsModule = angular.module('chApp.constants', []);

constantsModule.value('ChartOptions', { 
    grillGauge: {
        min: 0, max: 200,
        width: 300, height: 130,
        redFrom: 145, redTo: 200,
        yellowFrom: 135, yellowTo: 145,
        minorTicks: 5 
    },
    meatGauge: {
        min: 0, max: 110,
        width: 300, height: 130,
        redFrom: 90, redTo: 130,
        yellowFrom: 85, yellowTo: 90,
        minorTicks: 5
   },
   lineChart: {
        width: 700, height: 300,
        interpolateNulls: true,
        legend: {position:'top'},
        seriesType: "lines",
        series: {
            2: {
                type: "area",
                targetAxisIndex: 1 
            }
        },
        vAxes: [
            { title:'Temperature °C' },
            { 
                title:'Vent %',
                minValue: 0, maxValue: 100
            }
        ]
   }
});

constantsModule.value('Constants', {
    missingMeasurementValue: -100,
    temperatureEventType: 1,
    ventEventType: 2,
    controllerEventType: 3,
    defaultGrillTemperatureAlarmLimit: 15,
    alarmFrequency: 60000
});
