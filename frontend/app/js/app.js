'use strict';

// Declare app level module which depends on filters, and services
angular.module('chApp', [
  'ngRoute',
  'ngResource',
  'chApp.services',
  'chApp.directives',
  'chApp.controllers',
  'chApp.constants'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/operationView', {templateUrl: 'partials/operation.html', controller: 'OperationCtrl'});
  $routeProvider.otherwise({redirectTo: '/operationView'});
}]);
