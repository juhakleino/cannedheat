/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * db_handler.c
 *
 *  Created on: Jun 17, 2014
 *      Author: Juha Leino
 */

#include <string.h>
#include <db.h>
#include "db_handler.h"

// Types

typedef struct {
    ch_temperature_measurements_t temperature_measurements;
    double vent_state;
    controller_configuration_t controller_configuration;
} measurement_db_record_t;

// Constants
static char const * const DB_FILENAME = "/var/cannedheat/temperature_measurements";

/*!
 * Fills the database record with "don't care" values
 * @param record The DB record struct.
 */
void initialize_record(measurement_db_record_t *record) {
    record->temperature_measurements.grill_temperature = CH_MISSING_VALUE;
    record->temperature_measurements.meat_temperature = CH_MISSING_VALUE;
    record->vent_state = CH_MISSING_VALUE;
    record->controller_configuration.enabled_flag = CH_MISSING_VALUE;
    record->controller_configuration.grill_target_temperature = CH_MISSING_VALUE;
    record->controller_configuration.meat_target_temperature = CH_MISSING_VALUE;
}

/*
 * Call function to be called by db->err() function.
 * See Berkeley DB documentation for further information.
 */
void db_error_callback(
        const DB_ENV *dbenv,
        const char *errpfx,
        const char *msg) {
    WRITE_SYSLOG(LOG_ERR, msg, ""); // "" for getting rid of the gcc's warning
    TRACE(msg, "");
}

/*
 * Opens the DB connection and initializes the DB.
 * @param db_connection The DB connection handle (a pointer to a pointer)
 * @return STATUS_OK or STATUS_ERR
 */
int open_db(DB **db_connection) {
    int status = STATUS_OK;

    status = db_create(db_connection, NULL, 0);
    if (status != 0) {
        WRITE_SYSLOG(LOG_ERR, "Error in db_create(): %s\n", db_strerror(status));
        TRACE("db_create(): %s\n", db_strerror(status));
        status = STATUS_ERR;
    }
    if (0 == status) {
        status = (*db_connection)->set_flags(*db_connection, DB_DUP);
        if (status) {
            (*db_connection)->err(*db_connection, status, "open_db()\n");
            status = STATUS_ERR;
        }
    }
    if (0 == status) {
        (*db_connection)->set_errcall(*db_connection, db_error_callback);
        if ((status = (*db_connection)->open(*db_connection, NULL, DB_FILENAME, NULL,
                DB_BTREE, DB_CREATE, 0664)) != 0) {
            (*db_connection)->err(*db_connection, status, "Error during DB->open(%s)\n", DB_FILENAME);
            (*db_connection)->close(*db_connection, 0);
            *db_connection = NULL;
            status = STATUS_ERR;
        }
    }

    return status;
}

/*
 * DB connection clean-up.
 * @param db_connection The DB connection handle
 * @return STATUS_OK or STATUS_ERR
 */
void close_db(DB **db_connection) {
    int status = STATUS_OK;

    if (*db_connection) {
        status = (*db_connection)->close(*db_connection, 0);
        if (status) {
            (*db_connection)->err(*db_connection, status, "close()\n");
            status = STATUS_ERR;
        }
        *db_connection = NULL;
    }
}

/*
 * Stores the record to DB. The DB is opened and closed during each store operation.
 * @param timestamp The timestamp string to be used as the DB key
 * @param record The struct containing the data to be stored
 * @return STATUS_OK or STATUS_ERR
 */
int store_record_to_db(
        char *timestamp,
        measurement_db_record_t record) {
    int status = STATUS_OK;
    DB *db_connection = NULL;
    DBT key;
    DBT data;

    memset(&key, 0, sizeof(key));
    memset(&data, 0, sizeof(data));

    key.data = timestamp;
    key.size = strlen(timestamp) + 1;
    data.data = &record;
    data.size = sizeof(measurement_db_record_t);

    status = open_db(&db_connection);
    if (STATUS_OK == status) {
        status = db_connection->put(db_connection, NULL, &key, &data, 0 /*DB_NOOVERWRITE*/);
        if (status) {
            db_connection->err(db_connection, status, "DB->put()\n");
            status = STATUS_ERR;
        }
    }

    if (db_connection) {
        close_db(&db_connection);
    }

    return status;
}

/*
 * Stores the temperature measurement record to DB.
 * @param timestamp The timestamp string to be used as the DB key
 * @param measurements The struct containing temperature measurements
 * @return STATUS_OK or STATUS_ERR
 */
int store_measurements_to_db(
        char *timestamp,
        ch_temperature_measurements_t measurements) {
    int status = STATUS_OK;
    measurement_db_record_t record;

    initialize_record(&record);
    record.temperature_measurements = measurements;

    status = store_record_to_db(timestamp, record);

    return status;
}

/*
 * Stores the air vent state to DB.
 * @param timestamp The timestamp string to be used as the DB key
 * @param vent_state The state to be stored
 * @return STATUS_OK or STATUS_ERR
 */
int store_vent_state_to_db(
        char *timestamp,
        double vent_state) {
    int status = STATUS_OK;
    measurement_db_record_t record;

    initialize_record(&record);
    record.vent_state = vent_state;

    status = store_record_to_db(timestamp, record);

    return status;
}

/*!
 * Stores the temperature controller configuration to DB.
 * @param timestamp The timestamp string to be used as the DB key
 * @param configuration The configuration struct to be stored
 * @return STATUS_OK or STATUS_ERR
 */
int store_controller_configuration_to_db(
        char *timestamp,
        controller_configuration_t configuration) {
    int status = STATUS_OK;
    measurement_db_record_t record;

    initialize_record(&record);
    record.controller_configuration.enabled_flag = configuration.enabled_flag;
    record.controller_configuration.grill_target_temperature = configuration.grill_target_temperature;
    record.controller_configuration.meat_target_temperature = configuration.meat_target_temperature;

    status = store_record_to_db(timestamp, record);

    return status;
}

/*
 * Clears (truncates) the cooking session database
 * @return STATUS_OK or STATUS_ERR
 */
int clear_db() {
    int status = STATUS_OK;
    DB *db_connection = NULL;
    u_int32_t record_count = 0;

    TRACE("clear_db()\n");

    status = open_db(&db_connection);
    if (STATUS_OK == status) {
        status = db_connection->truncate(db_connection, NULL, &record_count, 0);
        if (status) {
            db_connection->err(db_connection, status, "DB->put()\n");
            status = STATUS_ERR;
        } else {
            TRACE("%i records deleted\n", record_count);
        }
    }

    if (db_connection) {
        close_db(&db_connection);
    }

    return status;
}
