/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * measurer.c
 *
 *  Created on: Jun 8, 2014
 *      Author: Juha Leino
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <glib.h>
#include <glib-unix.h>
#include "dbus_handler.h"
#include "db_handler.h"
#include "chlogger.h"

// Variables
static GMainLoop *main_loop = NULL;
static guint signal_source_ids[3];
static int logger_state_flag = 0;

/*
 * Forks a daemon process, prepares it and and exits from the parent process.
 * Exits (from all processes) in the case of errors.
 */
void daemonize(void) {
    pid_t pid;

    pid = fork();
    if (pid < 0) {
        perror("on fork while daemonizing");
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    // create a new session and put us into it
    if (setsid() < 0) {
        perror("on session creation while daemonizing");
        exit(EXIT_FAILURE);
    }

    // change the directory into root.
    if (chdir("/") < 0) {
        perror("on root dir change while daemonizing");
        exit(EXIT_FAILURE);
    }

    // reset umask
    umask(0);

    // close the normal file descriptors.
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}

/*
 * Signal handler for terminating signals
 */
static gboolean handle_quit_signal(__attribute__((unused)) gpointer user_data)
{
    TRACE("handle_quit_signal()\n");
    g_return_val_if_fail(main_loop != NULL, FALSE);
    if (main_loop) {
        g_main_loop_quit(main_loop);
    }

    return FALSE;
}

/*
 * Signal handler for SIGUSR1
 */
static gboolean handle_reload_signal(__attribute__((unused)) gpointer user_data) {
    int status = STATUS_OK;

    TRACE("handle_reload_signal()\n");
    status = read_server_configuration_file();
    WRITE_SYSLOG(LOG_INFO, "Configuration read with status %d\n", status);

    if (STATUS_OK == status) {
        // nothing yet
    } else {
        g_main_loop_quit(main_loop);
    }

    return TRUE;
}

/*
 * Registers the signal handlers
 */
void register_my_signal_handlers(GMainLoop *main_loop) {
    GSource *source = NULL;
    GMainContext *context = g_main_loop_get_context(main_loop);

    source = g_unix_signal_source_new(SIGUSR1);
    g_source_set_callback(source, handle_reload_signal, main_loop, NULL);
    signal_source_ids[0] = g_source_attach(source, context);

    source = g_unix_signal_source_new(SIGTERM);
    g_source_set_callback (source, handle_quit_signal, main_loop, NULL);
    signal_source_ids[1] = g_source_attach(source, context);

    source = g_unix_signal_source_new(SIGINT);
    g_source_set_callback (source, handle_quit_signal, main_loop, NULL);
    signal_source_ids[2] = g_source_attach(source, context);
}

/*
 * Logger state getter
 * @return Running/stopped state of the logger
 */
int logger_state() {
    return logger_state_flag;
}

/*
 * Logger state setter
 * @param new_state The new running/stopped state of the logger
 */
void set_logger_state(int new_state) {
    int old_state = logger_state();
    if (LOGGER_STATE_STOPPED == new_state || LOGGER_STATE_RUNNING == new_state) {
        logger_state_flag = new_state;

        if (logger_state_flag == LOGGER_STATE_RUNNING
                && old_state != logger_state_flag) {
            fetch_initial_vent_state();
            fetch_initial_controller_configuration();
        }
    }
}

/*!
 * Fetches the vent state over DBus and stores it to DB. To be used in the initialization of the
 * DB.
 */
void fetch_initial_vent_state() {
    int status = STATUS_OK;
    double vent_state = fetch_vent_state();
    time_t now = time(NULL);
    char time_buffer[20];

    memset(time_buffer, 0, sizeof(time_buffer));
    strftime(time_buffer, sizeof(time_buffer), DBUS_MEASUREMENT_TIME_STRING_FORMAT,
            localtime(&now));
    status = store_vent_state_to_db(time_buffer, vent_state);
    if (STATUS_OK != status) {
        TRACE("Could not store vent state to DB");
    }
}

/*!
 * Fetches the controller configuration over DBus and stores it to DB. To be used in
 * the initialization of the DB.
 */
void fetch_initial_controller_configuration() {
    int status = STATUS_OK;
    time_t now = time(NULL);
    char time_buffer[20];
    controller_configuration_t configuration;

    memset(&configuration, 0, sizeof(controller_configuration_t));
    memset(time_buffer, 0, sizeof(time_buffer));
    fetch_controller_configuration(&configuration);
    strftime(time_buffer, sizeof(time_buffer), DBUS_MEASUREMENT_TIME_STRING_FORMAT,
            localtime(&now));
    status = store_controller_configuration_to_db(time_buffer, configuration);
    if (STATUS_OK != status) {
        TRACE("Could not store controller configuration to DB");
    }
}

int main(int argc, char const *argv[]) {
    int status = STATUS_OK;

    status = initialize_configuration(argc, argv);

    if (STATUS_OK == status && !active_configuration()->debug_mode) {
        daemonize();
    }

    if (STATUS_OK == status) {
        connect_to_dbus();
        WRITE_SYSLOG(LOG_INFO, "Started successfully\n");
    }

    main_loop = g_main_loop_new(NULL, FALSE);
    register_my_signal_handlers(main_loop);

    TRACE("Going to the main loop\n");
    g_main_loop_run(main_loop);

    WRITE_SYSLOG(LOG_INFO, "Exits\n");

    if (main_loop) {
        g_main_loop_unref(main_loop);
    }

    return EXIT_SUCCESS;
}
