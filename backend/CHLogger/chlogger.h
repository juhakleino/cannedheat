/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * measurer.h
 *
 *  Created on: Jun 8, 2014
 *      Author: Juha Leino
 */

#ifndef CHLOGGER_H_
#define CHLOGGER_H_

#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <cannedheat.h>

static char const * const LOG_DAEMON_NAME="Canned Heat Logger";

static const int LOGGER_STATE_STOPPED = 0;
static const int LOGGER_STATE_RUNNING = 1;

typedef struct {
    int enabled_flag;
    double grill_target_temperature;
    double meat_target_temperature;
} controller_configuration_t;

int logger_state();
void set_logger_state(int new_state);
void fetch_initial_vent_state();
void fetch_initial_controller_configuration();

#endif /* CHLOGGER_H_ */
