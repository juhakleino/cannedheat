/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * dbus_handler.c
 *
 *  Created on: Jun 13, 2014
 *      Author: Juha Leino
 */

#include <string.h>
#define _XOPEN_SOURCE // for time.h/strptime()
#define __USE_XOPEN   // for time.h/strptime()
#include <time.h>
#include <gio/gio.h>
#include <cannedheat.h>
#include "db_handler.h"
#include "dbus_handler.h"

// Variables
static guint dbus_owner_id;
static GDBusConnection *dbus_connection = NULL;
static GDBusProxy *dbus_measurer_proxy = NULL;
static GDBusProxy *dbus_vent_proxy = NULL;
static GDBusProxy *dbus_controller_proxy = NULL;

// DBus interface definition
// -----------------------------------------------------------------------------------------

// Logger state property
static const GDBusPropertyInfo logger_state_property_info = {
        -1,
        DBUS_LOGGER_STATE_PROPERTY_NAME,
        "i",
        G_DBUS_PROPERTY_INFO_FLAGS_READABLE | G_DBUS_PROPERTY_INFO_FLAGS_WRITABLE,
        NULL
};

static const GDBusPropertyInfo *logger_property_info_pointers[] = {
        &logger_state_property_info,
        NULL
};

// Logger methods
static const GDBusMethodInfo logger_clear_method_info = {
        -1,
        DBUS_LOGGER_CLEAR_METHOD_NAME,
        NULL, NULL, // No arguments
        NULL
};

static const GDBusMethodInfo * const logger_method_info_pointers[] = {
     &logger_clear_method_info,
     NULL};


// Logger interface definition
static GDBusInterfaceInfo logger_interface_info = {
        -1,
        DBUS_LOGGER_INTERFACE_NAME,
        (GDBusMethodInfo **) &logger_method_info_pointers,
        NULL, // no signals
        (GDBusPropertyInfo **) &logger_property_info_pointers,
        NULL // no annotations
};

// Dbus logger object callbacks
/*
 * Dbus method callback. Checks the method name and calls corresponding internal function
 */
void handle_method_call(
        GDBusConnection *connection,
        const gchar *sender,
        const gchar *object_path,
        const gchar *interface_name,
        const gchar *method_name,
        GVariant *parameters,
        GDBusMethodInvocation *invocation,
        gpointer user_data) {
    TRACE("logger_method_call()\n");
    if (g_strcmp0(method_name, DBUS_LOGGER_CLEAR_METHOD_NAME) == 0) {
        (void) clear_db();
        g_dbus_method_invocation_return_value(invocation, NULL);
    }
}

/*
 * Called when measurer's state object is read over DBus
 * See GDbus documentation for the interface definition.
 */
static GVariant *handle_get_property(
        GDBusConnection  *connection,
        const gchar      *sender,
        const gchar      *object_path,
        const gchar      *interface_name,
        const gchar      *property_name,
        GError          **error,
        gpointer          user_data)
{
  GVariant *result;

  TRACE("handle_get_property()\n");
  result = g_variant_new_int32(logger_state());

  return result;
}

/*
 * Called when measurer's state object is set over DBus
 * See GDbus documentation for the interface definition.
 */
static gboolean handle_set_property(
        GDBusConnection  *connection,
        const gchar      *sender,
        const gchar      *object_path,
        const gchar      *interface_name,
        const gchar      *property_name,
        GVariant         *value,
        GError          **error,
        gpointer          user_data)
{
    int new_value = g_variant_get_int32(value);

    if (g_strcmp0(property_name, DBUS_LOGGER_STATE_PROPERTY_NAME) == 0) {
        TRACE("handle_set_property(): set state to %i\n", new_value);
        set_logger_state(new_value);
    }

    return TRUE;
}

static const GDBusInterfaceVTable logger_interface_vtable = {
        handle_method_call,
        handle_get_property,
        handle_set_property
};

/*
 * The function that is called when the measurer signal is received
 */
static void on_measurer_signal(
        GDBusProxy *proxy,
        gchar      *sender_name,
        gchar      *signal_name,
        GVariant   *parameters,
        gpointer    user_data)
{
    gchar *time_buffer;
    ch_temperature_measurements_t measurements;

    if (LOGGER_STATE_STOPPED == logger_state()) {
        return;
    }

    if (g_strcmp0(DBUS_MEASURER_TEMPERATURE_SIGNAL_NAME, signal_name) == 0) {
        g_variant_get(parameters, DBUS_MEASUREMENT_SIGNAL_FORMAT, &time_buffer,
                &measurements.grill_temperature, &measurements.meat_temperature);
        store_measurements_to_db(time_buffer, measurements);
        g_free(time_buffer);
    }
}

/*
 * The function that is called when the vent state signal is received
 */
static void on_vent_signal(
        GDBusProxy *proxy,
        gchar      *sender_name,
        gchar      *signal_name,
        GVariant   *parameters,
        gpointer    user_data)
{
    gchar *time_buffer;
    gboolean movement_finished;
    double vent_state = 0.0;

    if (LOGGER_STATE_STOPPED == logger_state()) {
        return;
    }

    if (g_strcmp0(DBUS_VENT_SIGNAL_NAME, signal_name) == 0) {
        g_variant_get(parameters, DBUS_VENT_SIGNAL_FORMAT, &time_buffer, &vent_state, &movement_finished);
        store_vent_state_to_db(time_buffer, vent_state);
        g_free(time_buffer);
    }
}

/*
 * The function that is called when the temperature controller configuration signal is received
 */
static void on_controller_signal(
        GDBusProxy *proxy,
        gchar      *sender_name,
        gchar      *signal_name,
        GVariant   *parameters,
        gpointer    user_data)
{
    gchar *time_buffer;
    controller_configuration_t configuration;

    if (LOGGER_STATE_STOPPED == logger_state()) {
        return;
    }

    TRACE("on_controller_signal()\n");
    memset(&configuration, 0, sizeof(controller_configuration_t));
    if (g_strcmp0(TEMPERATURE_CONTROLLER_SIGNAL_NAME, signal_name) == 0) {
        g_variant_get(parameters, TEMPERATURE_CONTROLLER_SIGNAL_FORMAT, &time_buffer,
            &configuration.enabled_flag, &configuration.grill_target_temperature,
            &configuration.meat_target_temperature);
        store_controller_configuration_to_db(time_buffer, configuration);
        g_free(time_buffer);
    }
}

/*
 * GDbus callback. Registers the logger object to DBus and creates proxies for the signal senders.
 */
static void on_bus_acquired(
        GDBusConnection *connection,
        const gchar     *name,
        gpointer         user_data)
{
    int status = STATUS_OK;
    guint registration_id;
    GError *local_error = NULL;
    GDBusProxyFlags flags;

    TRACE("on_bus_acquired()\n");
    dbus_connection = connection;

    registration_id = g_dbus_connection_register_object(connection, DBUS_LOGGER_OBJECT_NAME,
        &logger_interface_info, &logger_interface_vtable,
        NULL, NULL,  // user data and user data free function
        &local_error);

    g_assert(registration_id > 0);
    if (local_error) {
        WRITE_SYSLOG(LOG_ERR, "Error registering object to DBus: %s", local_error->message);
        g_error_free(local_error);
        status = STATUS_ERR;
    }

    flags = G_DBUS_PROXY_FLAGS_NONE // | G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES
            | G_DBUS_PROXY_FLAGS_DO_NOT_AUTO_START;

    if (STATUS_OK == status) {
        local_error = NULL;
        dbus_measurer_proxy = g_dbus_proxy_new_sync(dbus_connection, flags, NULL,
                DBUS_MEASURER_SERVER_NAME, DBUS_MEASURER_OBJECT_NAME,
                DBUS_MEASURER_INTERFACE_NAME, NULL, &local_error);
        if (dbus_measurer_proxy == NULL) {
            WRITE_SYSLOG(LOG_ERR, "Error creating measurer Dbus proxy: %s", local_error->message);
            g_error_free(local_error);
            status = STATUS_ERR;
        } else {
            g_signal_connect(dbus_measurer_proxy, "g-signal", G_CALLBACK(on_measurer_signal), NULL);
        }
    }
    if (STATUS_OK == status) {
        local_error = NULL;
        dbus_vent_proxy = g_dbus_proxy_new_sync(dbus_connection, flags, NULL,
                DBUS_VENT_SERVER_NAME, DBUS_VENT_OBJECT_NAME,
                DBUS_VENT_INTERFACE_NAME, NULL, &local_error);
        if (dbus_vent_proxy == NULL) {
            WRITE_SYSLOG(LOG_ERR, "Error creating vent Dbus proxy: %s", local_error->message);
            g_error_free(local_error);
            status = STATUS_ERR;
        } else {
            g_signal_connect(dbus_vent_proxy, "g-signal", G_CALLBACK(on_vent_signal), NULL);
        }
    }
    if (STATUS_OK == status) {
        local_error = NULL;
        dbus_controller_proxy = g_dbus_proxy_new_sync(dbus_connection, flags, NULL,
                TEMPERATURE_CONTROLLER_DBUS_SERVICE_NAME, TEMPERATURE_CONTROLLER_DBUS_OBJECT_NAME,
                TEMPERATURE_CONTROLLER_DBUS_INTERFACE_NAME, NULL, &local_error);
        if (dbus_controller_proxy == NULL) {
            WRITE_SYSLOG(LOG_ERR, "Error creating temperature controller Dbus proxy: %s", local_error->message);
            g_error_free(local_error);
            status = STATUS_ERR;
        } else {
            g_signal_connect(dbus_controller_proxy, "g-signal", G_CALLBACK(on_controller_signal), NULL);
        }
    }
}

/*
 * GDbus callback. Does nothing.
 */
static void on_name_acquired(
        GDBusConnection *connection,
        const gchar     *name,
        gpointer         user_data)
{
    TRACE("on_name_acquired()\n");
}

/*
 * GDbus callback. Does nothing.
 */
static void on_name_lost(
        GDBusConnection *connection,
        const gchar     *name,
        gpointer         user_data)
{
    TRACE("on_name_lost()\n");
}

/*
 * Connects to the Dbus system bus as a client
 */
void connect_to_dbus() {
    dbus_owner_id = g_bus_own_name(G_BUS_TYPE_SYSTEM, DBUS_LOGGER_SERVER_NAME,
            G_BUS_NAME_OWNER_FLAGS_NONE, on_bus_acquired, on_name_acquired,
            on_name_lost, NULL, NULL);
}

/*
 * Closes the Dbus connection down
 */
void close_dbus_connection() {
    g_bus_unown_name(dbus_owner_id);
    g_object_unref(dbus_measurer_proxy);
    g_object_unref(dbus_vent_proxy);
    g_object_unref(dbus_controller_proxy);
}

/*
 * Read vent state over DBus
 */
double fetch_vent_state() {
    double result = 0.0;
    GVariant *value;

    TRACE("fetch_vent_state()\n");

    value = g_dbus_proxy_get_cached_property(dbus_vent_proxy, DBUS_VENT_STATE_PROPERTY_NAME);

    if (value) {
        result = g_variant_get_double(value);
        g_variant_unref(value);
    }
    return result;
}

/*!
 * Read temperature controller configuration over DBus
 */
void fetch_controller_configuration(controller_configuration_t *configuration) {
    GVariant *value;

    TRACE("fetch_controller_configuration()\n");
    value = g_dbus_proxy_get_cached_property(dbus_controller_proxy,
        TEMPERATURE_CONTROLLER_ENABLED_PROPERTY_NAME);
    if (value) {
        configuration->enabled_flag = g_variant_get_boolean(value);
        g_variant_unref(value);
    }

    value = g_dbus_proxy_get_cached_property(dbus_controller_proxy,
        TEMPERATURE_CONTROLLER_GRILL_TARGET_TEMPERATURE_PROPERTY_NAME);
    if (value) {
        configuration->grill_target_temperature = g_variant_get_double(value);
        g_variant_unref(value);
    }

    value = g_dbus_proxy_get_cached_property(dbus_controller_proxy,
        TEMPERATURE_CONTROLLER_MEAT_TARGET_TEMPERATURE_PROPERTY_NAME);
    if (value) {
        configuration->meat_target_temperature = g_variant_get_double(value);
        g_variant_unref(value);
    }
}


