/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * dbus_handler.c
 *
 *  Created on: Jun 13, 2014
 *      Author: Juha Leino
 */

#include <string.h>
#include <gio/gio.h>
#include "configuration.h"
#include "dbus_handler.h"

// Variables
static guint dbus_owner_id;
static GDBusConnection *dbus_connection = NULL;

// Interface definition
// -----------------------------------------------------------------------------------------

// Temperature measurement signal
static const GDBusArgInfo measurer_temperature_measurement_signal_timestamp_arg = {
        -1,
        "timestamp",
        "s",
        NULL
};

static const GDBusArgInfo measurer_temperature_measurement_signal_grill_temperature_arg = {
        -1,
        "grillTemperature",
        "d",
        NULL
};

static const GDBusArgInfo measurer_temperature_measurement_signal_meat_temperature_arg = {
        -1,
        "meatTemperature",
        "d",
        NULL
};

static const GDBusArgInfo *measurer_temperature_measurement_signal_args_info_pointers[] = {
        &measurer_temperature_measurement_signal_timestamp_arg,
        &measurer_temperature_measurement_signal_grill_temperature_arg,
        &measurer_temperature_measurement_signal_meat_temperature_arg,
        NULL
};

static const GDBusSignalInfo measurer_temperature_measurement_signal_info = {
        -1,
        "TemperatureMeasurement",
        (GDBusArgInfo **) &measurer_temperature_measurement_signal_args_info_pointers,
        NULL // no annotations
};

static const GDBusSignalInfo *measurer_signal_info_pointers[] = {
        &measurer_temperature_measurement_signal_info,
        NULL
};

// Measurer interface definition
static GDBusInterfaceInfo measurer_interface_info = {
        -1,
        "fi.juhakleino.cannedheat.measurer",
        NULL, // no methods
        (GDBusSignalInfo **) &measurer_signal_info_pointers,
        NULL, // no properties
        NULL // no annotations
};

/*
 * GDbus callback. Registers the measurer object to DBus.
 */
static void on_bus_acquired(
        GDBusConnection *connection,
        const gchar     *name,
        gpointer         user_data)
{
    guint registration_id;
    GError *local_error = NULL;

    TRACE("on_bus_acquired()\n");
    dbus_connection = connection;

    registration_id = g_dbus_connection_register_object(connection, DBUS_MEASURER_OBJECT_NAME,
        &measurer_interface_info,
        NULL,        // no incoming calls allowed
        NULL, NULL,  // user data and user data free function
        &local_error);

    g_assert(registration_id > 0);
    if (local_error) {
        WRITE_SYSLOG(LOG_ERR, "Error registering object to DBus: %s", local_error->message);
        g_error_free(local_error);
    }
}

/*
 * GDbus callback. Does nothing.
 */
static void on_name_acquired(
        GDBusConnection *connection,
        const gchar     *name,
        gpointer         user_data)
{
    TRACE("on_name_acquired()\n");
}

/*
 * GDbus callback. Does nothing.
 */
static void on_name_lost(
        GDBusConnection *connection,
        const gchar     *name,
        gpointer         user_data)
{
    TRACE("on_name_lost()\n");
}

/*
 * Connects to the Dbus system bus as a client
 * @param loop the Glib main loop of the process
 * @return STATUS_OK or STATUS_ERR
 */
void connect_to_dbus() {
    dbus_owner_id = g_bus_own_name(G_BUS_TYPE_SYSTEM, DBUS_MEASURER_SERVER_NAME,
            G_BUS_NAME_OWNER_FLAGS_NONE, on_bus_acquired, on_name_acquired,
            on_name_lost, NULL, NULL);



//    GError *local_error = NULL;
//
//    dbus_connection = g_bus_get_sync(G_BUS_TYPE_SYSTEM, NULL, &local_error);
//
//    if (local_error) {
//        WRITE_SYSLOG(LOG_ERR, "Error in connecting to the system bus: %s",
//                local_error->message);
//        g_error_free(local_error);
//    }
}

/*
 * Closes the Dbus connection down
 */
void close_dbus_connection() {

//    GError *local_error = NULL;
//
//    g_dbus_connection_close_sync(dbus_connection, NULL, &local_error);
//
//    if (local_error) {
//        WRITE_SYSLOG(LOG_ERR, "Error in closing the system bus connection: %s",
//                local_error->message);
//        g_error_free(local_error);
//    }
//
//    g_free(dbus_connection);
}

/*
 * Sends the measurement values to the system bus as a signal
 * @param dbuscp Dbus connection pointer
 * @param timestamp Measurement timestamp (acts also as the identifier)
 * @param measurements Measured temperatures
 * @return STATUS_OK or STATUS_ERR
 */
int send_to_dbus(
        time_t timestamp,
        ch_temperature_measurements_t measurements) {
    int status = STATUS_OK;
    GError *local_error = NULL;
    char time_buffer[20];

    // Convert timestamp as string
    memset(time_buffer, 0, sizeof(time_buffer));
    strftime(time_buffer, sizeof(time_buffer), DBUS_MEASUREMENT_TIME_STRING_FORMAT,
            localtime(&timestamp));

    g_dbus_connection_emit_signal(dbus_connection, NULL,
            DBUS_MEASURER_OBJECT_NAME, DBUS_MEASURER_INTERFACE_NAME,
            DBUS_MEASURER_TEMPERATURE_SIGNAL_NAME,
            g_variant_new(DBUS_MEASUREMENT_SIGNAL_FORMAT, time_buffer, measurements.grill_temperature,
                    measurements.meat_temperature),
            &local_error);
    if (local_error) {
        WRITE_SYSLOG(LOG_ERR, "Error in sending measurement signal to Dbus: %s",
                local_error->message);
        status = STATUS_ERR;
        g_error_free(local_error);
    }

    return status;
}
