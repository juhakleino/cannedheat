/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * analog_input_reader.c
 *
 *  Created on: Jun 8, 2014
 *      Author: Juha Leino
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include "configuration.h"
#include "analog_input_reader.h"

// Constants

// Ideally the folders would be found with wildcards
//#define INPUT_FILE_TEMPLATE = "/sys/devices/ocp.3/helper.15/AIN%d";
#define INPUT_FILE_TEMPLATE "/sys/bus/iio/devices/iio:device0/in_voltage%d_raw"
static const int FIXED_RESISTANCE = 10000;


/*
 * Reads the analog input
 * @param input_port The analog input port between 0 and 6 (AIN0 - AIN6)
 * @param value The value read from the port (between 0 and 1800 mV)
 * @return STATUS_OK or STATUS_ERR. In the case of error, value is undefined
 */
int read_analog_input(int input_port, int *value) {
    int status = STATUS_OK;
    char input_file_name[sizeof(INPUT_FILE_TEMPLATE)];
    char *line;
    char *end_ptr;
    FILE * f;
    size_t bytes_allocated = 6;
    ssize_t bytes_read = 0;

    line = malloc(bytes_allocated + 1);
    memset(line, 0, bytes_allocated + 1);

    sprintf(input_file_name, INPUT_FILE_TEMPLATE, input_port);

    f = fopen(input_file_name, "r");

    if (NULL == f) {
        WRITE_SYSLOG(LOG_ERR, "Cannot open analog input file (errno: %d)\n", errno);
        status = STATUS_ERR;
    }

    if (STATUS_OK == status
        && (bytes_read = getline(&line, &bytes_allocated, f)) != -1) {
        *value = strtol(line, &end_ptr, 10);
        if (end_ptr == line) {
            WRITE_SYSLOG(LOG_ERR, "Cannot parse %s\n", input_file_name);
            status = STATUS_ERR;
        }
    } else {
        WRITE_SYSLOG(LOG_ERR, "Cannot read %s\n", input_file_name);
        status = STATUS_ERR;
    }

    if (f) {
        fclose(f);
    }
    free(line);

    return status;
}

/*
 * Reads the analog input and converts the result to temperature using Steinhart-Hart equation
 * @param input_port See read_analog_input()
 * @param temperature The processed measurement value
 * @return STATUS_OK or STATUS_ERR. In the case of error, value is undefined
 */
int read_temperature(int input_port, double *temperature) {
    int status = STATUS_OK;
    int analog_input_value;
    double V;
    double T;
    double R;

    status = read_analog_input(input_port, &analog_input_value);

    if (STATUS_OK == status) {
        V = 1800.0 * (double) analog_input_value/4095;
        R = (double) FIXED_RESISTANCE/((1800.0/V)-1.0);
        R = log(R);
        T = active_configuration()->steinhart_a
                + active_configuration()->steinhart_b * R
                + active_configuration()->steinhart_c * pow(R, 3);
        *temperature = 1/T - 273.15; // Kelvin -> Celsius
    }

    return status;
}

/*
 * Reads to values from the configured analog input ports
 * @param measurements The struct to which the measured values are stored
 * @return STATUS_OK or STATUS_ERR
 */
int read_measurements(ch_temperature_measurements_t *measurements) {
    int status = STATUS_OK;

    status = read_temperature(active_configuration()->grill_probe_port,
            &measurements->grill_temperature);

    if (STATUS_OK == status) {
        status = read_temperature(active_configuration()->meat_probe_port,
                &measurements->meat_temperature);
    }

    return status;
}

