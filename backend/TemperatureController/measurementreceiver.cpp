/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <QDebug>
#include <QDBusConnection>
#include <cannedheat.h>
#include <loggingutilities.h>
#include "measurementreceiver.h"

/*!
 * \brief Initializes the DBus listener for the temperature signals
 * \param parent
 */
MeasurementReceiver::MeasurementReceiver(QObject *parent) : QObject(parent) {
    mDbusInterface = new QDBusInterface(DBUS_MEASURER_SERVER_NAME, DBUS_MEASURER_OBJECT_NAME,
            DBUS_MEASURER_INTERFACE_NAME, QDBusConnection::systemBus(), this);
    mDbusInterface->connection().connect(DBUS_MEASURER_SERVER_NAME, DBUS_MEASURER_OBJECT_NAME,
            DBUS_MEASURER_INTERFACE_NAME, DBUS_MEASURER_TEMPERATURE_SIGNAL_NAME, this,
            SLOT(temperatureMeasurement(QString, double, double)));
    if (!mDbusInterface->isValid()) {
        LoggingUtilities::FatalError("Cannot connect to Measurer over DBus");
    }
}

/*!
 * \brief Receives the temperature measurement signals from DBus and forwards the grill
 *        temperature as a Qt signal
 * \param timestamp The timestamp of the measurement
 * \param grillTemperature The measured temperature of the grill
 * \param meatTemperature The measured temperature of the meat
 */
void MeasurementReceiver::temperatureMeasurement(
        QString timestamp,
        double grillTemperature,
        double meatTemperature) {
    qDebug() << "MeasurementReceiver::temperatureMeasurement()" << timestamp
                << grillTemperature << meatTemperature;
    emit grillTemperatureChanged(grillTemperature);
}
