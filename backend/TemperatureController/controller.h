/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>

class ControlVariableInterface {
public:
    virtual double getControlVariable() = 0;
};

class Controller : public QObject
{
    Q_OBJECT
public:
    explicit Controller(ControlVariableInterface *controlVariableManager, QObject *parent = 0);

public:
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled)
    Q_PROPERTY(double target READ target WRITE setTarget)
    Q_PROPERTY(double kc READ kc WRITE setKc)
    Q_PROPERTY(double ki READ ki WRITE setKi)
    Q_PROPERTY(double kd READ kd WRITE setKd)

    bool enabled();
    double target();
    double kc();
    void setKc(double newValue);
    double ki();
    void setKi(double newValue);
    double kd();
    void setKd(double newValue);

signals:
    void controlVariableChanged(double newValue);

public slots:
    void reconfigure();
    void setEnabled(bool newState);
    void setTarget(double newValue);

    void updateProcessVariable(double xk);

private:
    void reset();

private:
    ControlVariableInterface *mControlVariableManager;
    bool mEnabledFlag;

    // Settings
    double mTarget;
    double mKc;
    double mKi;
    double mKd;
    double mDeadband;

    // State
    double mXk_1;
    double mXk_2;
    double mYk_1;
    double mYOut;
    uint   mInitializationCounter;
};

#endif // CONTROLLER_H
