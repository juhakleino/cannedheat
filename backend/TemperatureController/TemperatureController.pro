#-------------------------------------------------
#
# Project created by QtCreator 2014-06-30T15:30:01
#
#-------------------------------------------------

QT       += core
QT       += dbus
QT       -= gui

TARGET = TemperatureController
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../Common

SOURCES += main.cpp \
    controllerinterfaceadaptor.cpp \
    controllerinterface.cpp \
    measurementreceiver.cpp \
    ventcontrollerproxy.cpp \
    controller.cpp \
    ../Common/loggingutilities.cpp \
    ../Common/cannedheatsettings.cpp \
    ../Common/unixsignalhandler.cpp

OTHER_FILES += \
    dbus_interface.xml \
    generate_dbus_adaptor.sh

HEADERS += \
    controllerinterfaceadaptor.h \
    controllerinterface.h \
    measurementreceiver.h \
    ventcontrollerproxy.h \
    controller.h \
    ../Common/cannedheat.h \
    ../Common/loggingutilities.h \
    ../Common/cannedheatsettings.h \
    ../Common/unixsignalhandler.h
