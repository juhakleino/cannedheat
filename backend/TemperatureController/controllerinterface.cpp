/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <QDebug>
#include <QDateTime>
#include <cannedheat.h>
#include <cannedheatsettings.h>
#include "controllerinterface.h"

ControllerInterface::ControllerInterface(Controller *controller)
    : QObject(controller), mController(controller) {
}

bool ControllerInterface::enabled() {
    qDebug() << "ControllerInterface::enabled()";
    return mController->enabled();
}

void ControllerInterface::setEnabled(bool enabled) {
    qDebug() << "ControllerInterface::setEnabled()" << enabled;
    if (enabled != this->enabled()) {
        sendConfigurationSignal();
        mController->setEnabled(enabled);
        emit stateChanged(enabled);
        sendConfigurationSignal();
    }
}

double ControllerInterface::grillTargetTemperature() {
    qDebug() << "ControllerInterface::grillTargetTemperature()";
    return CannedheatSettings::instance().grillTargetTemperature();
}

void ControllerInterface::setGrillTargetTemperature(double targetTemperature) {
    qDebug() << "ControllerInterface::setGrillTargetTemperature()" << targetTemperature;
    if (targetTemperature != grillTargetTemperature()) {
        sendConfigurationSignal();
        emit grillTargetTemperatureChanged(targetTemperature);
        sendConfigurationSignal();
    }
}

double ControllerInterface::meatTargetTemperature() {
    qDebug() << "ControllerInterface::meatTargetTemperature()";
    return CannedheatSettings::instance().meatTargetTemperature();
}

void ControllerInterface::setMeatTargetTemperature(double targetTemperature) {
    qDebug() << "ControllerInterface::setMeatTargetTemperature()" << targetTemperature;
    if (targetTemperature != meatTargetTemperature()) {
        sendConfigurationSignal();
        emit meatTargetTemperatureChanged(targetTemperature);
        sendConfigurationSignal();
    }
}

void ControllerInterface::sendConfigurationSignal() {
    emit configurationChanged(QDateTime::currentDateTime().toString(SIGNAL_TIME_FORMAT),
        this->enabled(), this->grillTargetTemperature(), this->meatTargetTemperature());
}
