/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef VENTCONTROLLERPROXY_H
#define VENTCONTROLLERPROXY_H

#include <QObject>
#include <QDBusInterface>
#include "controller.h"

class VentControllerProxy : public QObject, public ControlVariableInterface {
    Q_OBJECT
public:
    explicit VentControllerProxy(QObject *parent = 0);

public:
    double getState();

public: // from ControlVariableInterface
    virtual double getControlVariable();

public slots:
    void setState(double newState);

private:
    QDBusInterface *mDbusInterface;
};

#endif // VENTCONTROLLERPROXY_H
