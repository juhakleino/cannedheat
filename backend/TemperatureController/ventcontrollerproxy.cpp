/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <QDebug>
#include <QDBusMessage>
#include <QVariant>
#include <cannedheat.h>
#include "ventcontrollerproxy.h"

/*!
 * \brief Initializes the DBus interface for Vent object
 * \param parent
 */
VentControllerProxy::VentControllerProxy(QObject *parent) :
    QObject(parent) {

    mDbusInterface = new QDBusInterface(DBUS_VENT_SERVER_NAME, DBUS_VENT_OBJECT_NAME,
            DBUS_VENT_INTERFACE_NAME, QDBusConnection::systemBus(), this);
}

/*!
 * \brief Reads the state property of the vent over DBus
 * \return The state of the vent
 */
double VentControllerProxy::getState() {
    double result = mDbusInterface->property(DBUS_VENT_STATE_PROPERTY_NAME).toDouble();
    qDebug() << "VentControllerProxy::getState()" << result;
    return result;
}

/*!
 * \brief The controller callback method for fetching the vent state
 * \return See getState() methd
 */
double VentControllerProxy::getControlVariable() {
    return getState();
}

/*!
 * \brief Sets the state property of the vent over DBus
 * \param newState The new (requested) state of the vent
 */
void VentControllerProxy::setState(double newState) {
    qDebug() << "VentControllerProxy::setState()" << newState;
    mDbusInterface->setProperty(DBUS_VENT_STATE_PROPERTY_NAME, QVariant(newState));
}

