/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <math.h>
#include <QDebug>
#include "cannedheatsettings.h"
#include "controller.h"

// Constants
static const double GRILL_TARGET_TEMPERATURE_DEFAULT = 125.0;
static const double CONTROL_VARIABLE_MAX = 100.0;
static const double CONTROL_VARIABLE_MIN = 0.0;

Controller::Controller(ControlVariableInterface *controlVariableManager, QObject *parent)
    : QObject(parent), mControlVariableManager(controlVariableManager), mEnabledFlag(false)
    , mTarget(GRILL_TARGET_TEMPERATURE_DEFAULT) {
    reconfigure();
}

/*
 * PID controller algorithm implementation
 * @param xk The latest measured value of the process variable
 * @return The new value of the control variable
 */
void Controller::updateProcessVariable(double xk) {
    if (!mEnabledFlag) {
        // Controller disabled, return immediately
        return;
    } else if (mInitializationCounter) {
        // The initialization going on, store process variable values
        mInitializationCounter--;
        mXk_2 = mXk_1;
        mXk_1 = xk;
        if (0 == mInitializationCounter) {
            // The last initialization step: read the current air vent state
            mYk_1 = mControlVariableManager->getControlVariable();
            mYOut = mYk_1;
        }
        return; // Initialization on-going, return before actual PID calculation
    }

    double yc = mKc*(mXk_1-xk);           // P term
    double yi = mKi*(mTarget-xk);         // I term
    double yd = mKd*(2.0*mXk_1-xk-mXk_2); // D term

    double yk = mYk_1+yc+yi+yd;           // New control variable

    qDebug() << "Control cycle: xk=" << xk << "xk_1=" << mXk_1 << "xk_2=" << mXk_2;
    qDebug() << "PID terms: yk_1=" << mYk_1;
    qDebug() << "\tP: " << yc;
    qDebug() << "\tI: " << yi;
    qDebug() << "\tD: " << yd;
    qDebug() << "==> yk = " << yk;

    // Set the control variable to the valid value range
    if (yk > CONTROL_VARIABLE_MAX) {
        yk = CONTROL_VARIABLE_MAX;
    } else if (yk < CONTROL_VARIABLE_MIN) {
        yk = CONTROL_VARIABLE_MIN;
    }

    // Update the state variables
    mXk_2 = mXk_1;
    mXk_1 = xk;
    mYk_1 = yk;

    // Emit the new control variable value, if it's outside the deadband or if
    // the limit of the control value range is reached
    if (mEnabledFlag
        && (fabs(yk-mYOut) > mDeadband/2
            || (yk <= CONTROL_VARIABLE_MIN && mYOut > CONTROL_VARIABLE_MIN)
            || (yk >= CONTROL_VARIABLE_MAX && mYOut < CONTROL_VARIABLE_MAX))) {
        mYOut = yk;
        emit controlVariableChanged(yk);
    }
}

/*
 * Read PID parameters from settings
 */
void Controller::reconfigure() {
    qDebug() << "Controller::reconfigure()";
    mKc = CannedheatSettings::instance().kc();
    mKi = CannedheatSettings::instance().ki();
    mKd = CannedheatSettings::instance().kd();
    mDeadband = CannedheatSettings::instance().deadband();
    qDebug() << "Controller::reconfigure(): kc=" << mKc << ", ki=" << mKi << ", kd=" << mKd;
    reset();
}

/*
 * @return true/false value indicating whether the controller is enabled or not
 */
bool Controller::enabled() {
    return mEnabledFlag;
}

/*
 * Enables or disables the controller
 * @param newState true, enables the controller, false disables
 */
void Controller::setEnabled(bool newState) {
    qDebug() << "Controller::setEnabled()" << newState;
    mEnabledFlag = newState;
    if (newState) {
        reset();
    }
}

/*
 * @return The target process variable value
 */
double Controller::target() {
    return mTarget;
}

/*
 * Set the target value of the process variable
 * @param newValue the new target value
 */
void Controller::setTarget(double newValue) {
    mTarget = newValue;
}

/*
 * @return Proportional gain of the controller
 */
double Controller::kc() {
    return mKc;
}

/*
 * @param newValue the new value of the proportional gain
 */
void Controller::setKc(double newValue) {
    mKc = newValue;
}

/*
 * @return Integral gain of the controller
 */
double Controller::ki() {
    return mKi;
}

/*
 * @param newValue the new value of the integral gain
 */
void Controller::setKi(double newValue) {
    mKi = newValue;
}

/*
 * @return Derivative gain of the controller
 */
double Controller::kd() {
    return mKd;
}

/*
 * @param newValue the new value of the derivative gain
 */
void Controller::setKd(double newValue) {
    mKd = newValue;
}

/*
 * Resets the internal state of the controller
 */
void Controller::reset() {
    mXk_1 = 0.0;
    mXk_2 = 0.0;
    mYk_1 = 0.0;
    mInitializationCounter = 2;
}


