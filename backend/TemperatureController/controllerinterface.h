/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef CONTROLLERINTERFACE_H
#define CONTROLLERINTERFACE_H

#include <QObject>
#include "controller.h"

/*!
 * \brief The implementation of the DBus interface of the temperature controller
 */
class ControllerInterface : public QObject
{
    Q_OBJECT
public:
    explicit ControllerInterface(Controller *controller = 0);

public:
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled)
    Q_PROPERTY(double grillTargetTemperature READ grillTargetTemperature WRITE setGrillTargetTemperature)
    Q_PROPERTY(double meatTargetTemperature READ meatTargetTemperature WRITE setMeatTargetTemperature)

    bool enabled();
    void setEnabled(bool enabled);

    double grillTargetTemperature();
    void setGrillTargetTemperature(double grillTargetTemperature);
    double meatTargetTemperature();
    void setMeatTargetTemperature(double meatTargetTemperature);

signals:
    void configurationChanged(
            QString timestamp,
            bool enable,
            double grillTargetTemperature,
            double meatTargetTemperature);
    void stateChanged(bool enabled);
    void grillTargetTemperatureChanged(double grillTargetTemperature);
    void meatTargetTemperatureChanged(double meatTargetTemperature);

private:
    void sendConfigurationSignal();

private:
    Controller *mController;
};

#endif // CONTROLLERINTERFACE_H
