/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <cannedheatsettings.h>
#include "gpiopin.h"

// Constants
#define GPIO_FOLDER "/sys/class/gpio/"
#define GPIO_EXPORT_FILE "export"
#define GPIO_UNEXPORT_FILE "unexport"
#define GPIO_PIN_FOLDER_NAME_PREFIX "gpio"
#define GPIO_DIRECTION_FILE_NAME "direction"
#define GPIO_FILE_DIRECTION_IN "in"
#define GPIO_FILE_DIRECTION_OUT "out"
#define GPIO_VALUE_FILE_NAME "value"
#define GPIO_VALUE_LOW "0"
#define GPIO_VALUE_HIGH "1"

GpioPin::GpioPin(uint pin, QObject *parent) :
        QObject(parent), mPinNumber(pin), mCurrentDirection(UNDEFINED_DIRECTION),
        mSimulationValue(PIN_LOW) {
    mGpioPinFolder = GPIO_FOLDER;
    mGpioPinFolder += GPIO_PIN_FOLDER_NAME_PREFIX;
    mGpioPinFolder += QString::number(mPinNumber);
    mGpioPinFolder += "/";
}

GpioPin::~GpioPin() {
    unexportPin();
}

int GpioPin::initialize() {
    int status = 0;

    status = exportPin();

    return status;
}

/*
 * Checks if the state of the GPIO pin (low/high)
 * @param state The current state of the pin
 * @return 0 if OK
 */
int GpioPin::value(PinState &state) {
    int status = 0;

    if (CannedheatSettings::instance().simulationModeOn()) {
        return mSimulationValue;
    }

    if (!(status = setDirection(INPUT_PIN))) {
        QFile valueFile(mGpioPinFolder + GPIO_VALUE_FILE_NAME);
        if (!valueFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
            qWarning() << "Open() failed for " << mGpioPinFolder + GPIO_VALUE_FILE_NAME
                << ", error " << valueFile.error();
            status = -1;
        }
        if (0 == status) {
            QTextStream inStream(&valueFile);
            QString content = inStream.readAll();

            if (0 == content.compare(GPIO_VALUE_LOW)) {
                state = PIN_LOW;
            } else if (0 == content.compare(GPIO_VALUE_HIGH)) {
                state = PIN_HIGH;
            }
            valueFile.close();
        }
    }

    return status;
}

/*
 * Sets the GPIO pin to the new state (low/high)
 * @param newValue The requested state of the pin
 * @return 0 if OK
 */
int GpioPin::setValue(PinState newValue) {
    int status = 0;

    if (CannedheatSettings::instance().simulationModeOn()) {
        mSimulationValue = newValue;
        return status;
    }

    if (!(status = setDirection(OUTPUT_PIN))) {
        QFile valueFile(mGpioPinFolder + GPIO_VALUE_FILE_NAME);
        if (!valueFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
            qWarning() << "Open() failed for " << mGpioPinFolder + GPIO_VALUE_FILE_NAME
                << ", error " << valueFile.error();
            status = -1;
        }
        if (0 == status) {
            QTextStream outStream(&valueFile);
            if (PIN_LOW == newValue) {
                outStream << GPIO_VALUE_LOW;
            } else {
                outStream << GPIO_VALUE_HIGH;
            }
            valueFile.flush();
            valueFile.close();
        }
    }

    return status;
}

/*
 * Makes the pin folder visible in the file system
 * @return 0 if OK
 */
int GpioPin::exportPin() {
    int status = 0;

    if (CannedheatSettings::instance().simulationModeOn()) {
        return status;
    }

    QFile exportFile(GPIO_FOLDER GPIO_EXPORT_FILE);

    if (!exportFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        status = -1;
    }

    if (0 == status) {
        QTextStream outStream(&exportFile);
        outStream << mPinNumber;

        exportFile.flush();
        exportFile.close();
    }

    return status;
}

/*
 * Removes the pin folder from the file system
 * @return 0 if OK
 */
int GpioPin::unexportPin() {
    int status = 0;

    if (CannedheatSettings::instance().simulationModeOn()) {
        return status;
    }

    QFile exportFile(GPIO_FOLDER GPIO_UNEXPORT_FILE);

    if (!exportFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        status = -1;
    }

    if (0 == status) {
        QTextStream outStream(&exportFile);
        outStream << mPinNumber;

        exportFile.flush();
        exportFile.close();
    }

    return status;
}

/*
 * Sets the in/out direction of the pin
 * @param newDirection The requested direction of the pin
 * @return 0 if OK
 */
int GpioPin::setDirection(Direction newDirection) {
    int status = 0;

    if (CannedheatSettings::instance().simulationModeOn()) {
        mCurrentDirection = newDirection;
        return status;
    }

    if (newDirection != mCurrentDirection
            && UNDEFINED_DIRECTION != newDirection) {
        QFile directionFile(mGpioPinFolder + GPIO_DIRECTION_FILE_NAME);
        if (!directionFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
            qWarning() << "Open() failed for " << mGpioPinFolder + GPIO_DIRECTION_FILE_NAME
                << ", error " << directionFile.error();
            status = -1;
        }
        if (0 == status) {
            QTextStream outStream(&directionFile);
            if (INPUT_PIN == newDirection) {
                outStream << GPIO_FILE_DIRECTION_IN;
            } else {
                outStream << GPIO_FILE_DIRECTION_OUT;
            }
            directionFile.flush();
            directionFile.close();
            mCurrentDirection = newDirection;
        }
    }

    return status;
}
