/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef GPIOPIN_H
#define GPIOPIN_H

#include <QObject>
#include <QString>

class GpioPin : public QObject
{
    Q_OBJECT
public:
    explicit GpioPin(uint pin, QObject *parent = 0);
    ~GpioPin();

public:
    int initialize();

public:
    enum PinState { PIN_LOW = 0, PIN_HIGH = 1 };
    int value(PinState &state);
    int setValue(PinState newValue);

signals:

public slots:

private:
    enum Direction { INPUT_PIN = 0, OUTPUT_PIN, UNDEFINED_DIRECTION };
    int exportPin();
    int unexportPin();
    int setDirection(Direction newDirection);

private:
    uint mPinNumber;
    QString mGpioPinFolder;
    Direction mCurrentDirection;

    PinState mSimulationValue;
};

#endif // GPIOPIN_H
