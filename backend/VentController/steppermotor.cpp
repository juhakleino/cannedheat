/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <QDebug>
#include <cannedheatsettings.h>
#include <loggingutilities.h>
#include "steppermotor.h"

// Constants
const ulong UPDATE_SIGNAL_INTERVAL = 3000; // ms

StepperMotor::StepperMotor(
        ulong initialPosition,
        QObject *parent)
    : QObject(parent), mPosition(initialPosition), mUpdateInterval(0),
      mDirection(UNDEFINED) {

    mStepTimer = new QTimer(this);
    connect(mStepTimer, SIGNAL(timeout()), this, SLOT(step()));
    mEasyDriver = new EasyDriver(this);
}

int StepperMotor::initialize() {
    int status = 0;

    if (!(status = mEasyDriver->initialize())) {
         status = mEasyDriver->setSleepingState(true);
    }

    return status;
}

/*
 * @return The number of steps the motor takes to rotate once
 */
ulong StepperMotor::stepsPerRotation() {
    return CannedheatSettings::instance().stepsPerRotation();
}

/*
 * The position of the stepper motor presented as steps from the 'point zero'
 */
ulong StepperMotor::position() const {
    return mPosition;
}

/*
 * Command the stepper motor to move
 * @param steps The number of steps to take
 * @return 0 if OK
 */
int StepperMotor::move(long steps) {
    int status = 0;
    Direction oldDirection = mDirection;

    if (steps > 0) {
        mDirection = COUNTERCLOCKWISE;
    } else if (steps < 0) {
        mDirection = CLOCKWISE;
    }

    if (steps) {
        stop();
        if (oldDirection != mDirection) {
            if (mEasyDriver->setDirection(CLOCKWISE == mDirection)) {
                LoggingUtilities::FatalError("Cannot change the direction of the stepper motor");
            }
        }

        mStepsToTake = abs(steps);
        status = startStepping();
    }
    return status;
}

/*
 * The function stops the current movement by setting the mStepsToTake counter to zero and
 * by stopping the step timer. It puts the stepper motor to sleep and sends the movementFinished signal.
 * @return 0 if OK
 */
int StepperMotor::stop() {
    int status = 0;

    if (mStepTimer->isActive()) {
        mStepTimer->stop();

        if (0 == mStepsToTake) {
            emit moveFinished(mPosition);
        }

        mStepsToTake = 0;
        status = mEasyDriver->setSleepingState(true);
    }

    return status;
}

/*
 * The slot that is called frequently by the step timer during the movement. It checks if the requested
 * movement is finished. If not, takes one step and updates the step counter and position variables. Otherwise,
 * it stops the step counter. The positionChanged signal is emitted during the movement at regular intervals.
 */
void StepperMotor::step() {
    if (mStepsToTake > 0) {
        mStepsToTake--;
        mPosition += mDirection;

        if (mEasyDriver->step()) {
            LoggingUtilities::FatalError("Easy Driver stepping function error");
        }

        if (0 == (mStepsToTake % mUpdateInterval) && 0 != mStepsToTake) {
            emit positionChanged(mPosition);
        }
    } else {
        if (stop()) {
            LoggingUtilities::FatalError("Stepper motor stop failed");
        }
    }
}

/*
 * Function which initializes the movement. Calculates the delay between the steps
 * to reach the right rotation speed. Wakes up the stepper motor and starts the step timer.
 */
int StepperMotor::startStepping() {
    int status = 0;

    // The delay between steps is a time divided by the steps to take;
    // i.e. a minute / (rotations per minute * steps per a rotation)
    // This is not completely accurate because the time spend to take each step
    // but a simple approximation
    int stepDelay = 60000 / (CannedheatSettings::instance().stepperMotorRpm()
        * CannedheatSettings::instance().stepsPerRotation());
    mStepTimer->setInterval(stepDelay);

    mUpdateInterval = UPDATE_SIGNAL_INTERVAL / stepDelay;

    if (!(status = mEasyDriver->setSleepingState(false))) {
        mStepTimer->start();
    }
    return status;
}
