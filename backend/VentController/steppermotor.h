/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef STEPPERMOTOR_H
#define STEPPERMOTOR_H

#include <QObject>
#include <QTimer>
#include "easydriver.h"

class StepperMotor : public QObject
{
    Q_OBJECT
public:
    explicit StepperMotor(
            ulong initialPosition,
            QObject *parent = 0);

public:
    int initialize();

public: // Properties
    Q_PROPERTY(ulong stepsPerRotation READ stepsPerRotation CONSTANT)
    Q_PROPERTY(ulong position READ position NOTIFY positionChanged)

    ulong stepsPerRotation();
    ulong position() const;

signals:
    void positionChanged(ulong newPosition);
    void moveFinished(ulong newPosition);

public slots:
    int move(long steps);
    int stop();

private slots:
    void step();

private:
    int startStepping();

private:
    enum Direction { UNDEFINED = 0, CLOCKWISE = -1, COUNTERCLOCKWISE = 1 };

private:
    EasyDriver *mEasyDriver;
    QTimer *mStepTimer;

    ulong mPosition;
    ulong mStepsToTake;
    ulong mUpdateInterval;
    Direction mDirection;
};

#endif // STEPPERMOTOR_H
