/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef LINEARACTUATOR_H
#define LINEARACTUATOR_H

#include <QObject>
#include "steppermotor.h"

class LinearActuator : public QObject
{
    Q_OBJECT
public:
    explicit LinearActuator(QObject *parent = 0);

public:
    int initialize();

public: // Properties
    Q_PROPERTY(double maxLength READ maxLength)
    Q_PROPERTY(double realPosition READ realPosition NOTIFY realPositionChanged)
    Q_PROPERTY(double targetPosition READ targetPosition WRITE setTargetPosition)

    double maxLength() const;
    double realPosition() const;
    double targetPosition() const;
    void setTargetPosition(double newValue);

signals:
    void realPositionChanged(double newPosition);

private slots:
    void updateRealPosition(ulong stepperMotorPosition);
    void moveCompleted(ulong stepperMotorPosition);

private:
    static ulong convertPositionToSteps(double position);
    static double convertStepsToPosition(ulong steps);

private:
    StepperMotor *mStepperMotor;
    double mTargetPosition;
};

#endif // LINEARACTUATOR_H
