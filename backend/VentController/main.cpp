/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <QCoreApplication>
#include <QtDBus/QDBusConnection>
#include <QDebug>
#include <QCommandLineParser>
#include <cannedheatsettings.h>
#include <loggingutilities.h>
#include <unixsignalhandler.h>
#include <cannedheat.h>
#include "vent.h"
#include "ventdbusadaptor.h"

#define VENT_CONTROLLER_APP_NAME "Canned Heat Vent Controller"

/*
 * Forks a daemon process, prepares it and and exits from the parent process.
 * Exits (from all processes) in the case of errors.
 */
void daemonize(void) {
    pid_t pid;

    pid = fork();
    if (pid < 0) {
        LoggingUtilities::FatalError("on fork while daemonizing");
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    // create a new session and put us into it
    if (setsid() < 0) {
        LoggingUtilities::FatalError("on session creation while daemonizing");
    }

    // change the directory into root.
    if (chdir("/") < 0) {
        LoggingUtilities::FatalError("on root dir change while daemonizing");
    }

    // reset umask
    umask(0);

    // close the normal file descriptors.
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}

int main(int argc, char *argv[])
{
    int status;
    QCoreApplication app(argc, argv);
    UnixSignalHandler signalHandler(&app);
    Vent *vent = new Vent(&app);
    new VentDbusAdaptor(vent);

    // Command line check
    QCommandLineParser parser;
    QCommandLineOption debugOption(QStringList() << "d" << "debug", "Debug (tracing) mode)");
    parser.addOption(debugOption);
    QCommandLineOption simulationOption(QStringList() << "s" << "simulate", "Simulation mode");
    parser.addOption(simulationOption);
    parser.process(app);
    if (!parser.isSet(debugOption) && !parser.isSet(simulationOption)) {
        daemonize();
    }
    CannedheatSettings::instance().setSimulationModeOn(parser.isSet(simulationOption));

    LoggingUtilities::setAppName(VENT_CONTROLLER_APP_NAME);
    LoggingUtilities::Syslog(LOG_INFO, "Started");

    if (!QDBusConnection::systemBus().isConnected()) {
        LoggingUtilities::FatalError("Cannot connect to the D-Bus system bus.");
    }

    if (!QDBusConnection::systemBus().registerService(DBUS_VENT_SERVER_NAME)) {
        LoggingUtilities::FatalError(QString("Cannot register the service: ")
            + QDBusConnection::systemBus().lastError().message());
    }

    if (!QDBusConnection::systemBus().registerObject(DBUS_VENT_OBJECT_NAME, vent)) {
        LoggingUtilities::FatalError(QString("Cannot register the object to Dbus:")
            + QDBusConnection::systemBus().lastError().message());
    }

    if (vent->initialize()) {
        LoggingUtilities::FatalError("Could not initialize the vent object");
    }

    UnixSignalHandler::setupUnixSignalHandlers();

    status = app.exec();

    LoggingUtilities::Syslog(LOG_INFO, "Exits");
    CannedheatSettings::free();
    LoggingUtilities::free();

    return status;
}
