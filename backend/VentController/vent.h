/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef STATEMANAGER_H
#define STATEMANAGER_H

#include <QObject>
#include "linearactuator.h"

class Vent : public QObject
{
    Q_OBJECT
public:
    explicit Vent(QObject *parent = 0);

public:
    int initialize();

public: // Properties
    Q_PROPERTY(double state READ state WRITE setState)
    double state() const;
    void setState(double value);

Q_SIGNALS: // SIGNALS
    void stateChanged(QString timestamp, double newValue, bool movementFinished);

public slots:

private slots:
    void actuatorUpdate(double newPosition);

private:
    void sendPositionSignal(bool movementFinished);
    double convertStateToPosition(double ventState) const;
    double convertPositionToState(double actuatorPosition) const;

private:
    LinearActuator *mActuator;
    // The state of the vent that is visible in Dbus (to other processes)
    double mState;
};

#endif // STATEMANAGER_H
