#-------------------------------------------------
#
# Project created by QtCreator 2014-06-19T09:24:01
#
#-------------------------------------------------

QT += core
QT += dbus
QT -= gui

TARGET = VentController
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../Common

SOURCES += main.cpp \
    ventdbusadaptor.cpp \
    linearactuator.cpp \
    steppermotor.cpp \
    vent.cpp \
    easydriver.cpp \
    gpiopin.cpp \
    ../Common/cannedheatsettings.cpp \
    ../Common/loggingutilities.cpp \
    ../Common/unixsignalhandler.cpp

HEADERS += \
    ventdbusadaptor.h \
    linearactuator.h \
    steppermotor.h \
    vent.h \
    easydriver.h \
    gpiopin.h \
    ../Common/cannedheatsettings.h \
    ../Common/loggingutilities.h \
    ../Common/unixsignalhandler.h \
    ../Common/cannedheat.h
