/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <cannedheatsettings.h>
#include "easydriver.h"

EasyDriver::EasyDriver(QObject *parent) :
    QObject(parent) {
    mStepPin = new GpioPin(CannedheatSettings::instance().stepGpioPin(), this);
    mSleepPin = new GpioPin(CannedheatSettings::instance().sleepGpioPin(), this);
    mDirectionPin = new GpioPin(CannedheatSettings::instance().directionGpioPin(), this);
}

int EasyDriver::initialize() {
    int status = 0;

    status = mStepPin->initialize();
    if (0 == status) {
        status = mSleepPin->initialize();
    }
    if (0 == status) {
        status = mDirectionPin->initialize();
    }

    return status;
}

/*
 * Checks if the stepper motor is sleeping
 * @param state The state of the motor
 * @return 0 if OK
 */
int EasyDriver::isSleeping(bool &state) {
    int status = 0;
    GpioPin::PinState pinValue;

    status = mSleepPin->value(pinValue);

    if (GpioPin::PIN_HIGH == pinValue) {
        state = true;
    } else {
        state = false;
    }

    return status;
}

/*
 * Sets the sleeping state of the stepper motor
 * @param newState The requested state of the motor
 * @return 0 if OK
 */
int EasyDriver::setSleepingState(bool newState) {
    int status = 0;

    status = mSleepPin->setValue(newState ? GpioPin::PIN_LOW : GpioPin::PIN_HIGH);

    return status;
}

/*
 * Checks if the motor is set to rotate clockwise or counterclockwise
 * @param isRotatingClockwise the flag which tells the direction
 * @return 0 if OK
 */
int EasyDriver::direction(bool &isRotatingClockwise) {
    int status = 0;
    GpioPin::PinState pinValue;

    status = mDirectionPin->value(pinValue);

    if (GpioPin::PIN_HIGH == pinValue) {
        isRotatingClockwise = true;
    } else {
        isRotatingClockwise = false;
    }

    return status;
}

/*
 * Sets the motor rotate clockwise or counterclockwise
 * @param willBeRotatingClockwise the flag which tells the direction
 * @return 0 if OK
 */
int EasyDriver::setDirection(bool willBeRotatingClockwise) {
    int status = 0;

    status = mDirectionPin->setValue(
                willBeRotatingClockwise ? GpioPin::PIN_HIGH : GpioPin::PIN_LOW);

    return status;
}

/*
 * Commands the stepper motor to take one step
 * @return 0 if OK
 */
int EasyDriver::step() {
    int status = 0;

    if (!(status = mStepPin->setValue(GpioPin::PIN_LOW))) {
        status = mStepPin->setValue(GpioPin::PIN_HIGH);
    }

    return status;
}
