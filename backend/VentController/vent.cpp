/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <QtDebug>
#include <QDateTime>
#include <cannedheat.h>
#include <cannedheatsettings.h>
#include "vent.h"

Vent::Vent(QObject *parent) :
        QObject(parent), mState(0.0) {
    mActuator = new LinearActuator(this);
    connect(mActuator, SIGNAL(realPositionChanged(double)), this, SLOT(actuatorUpdate(double)));
}

int Vent::initialize() {
    int status = 0;

    status = mActuator->initialize();

    if (!status) {
        sendPositionSignal(true);
    }

    return status;
}

/*
 * @return The state of the vent which indicates how open it is in the scale of 0-100%
 */
double Vent::state() const {
    return mState;
}

/*
 * Set the new target position for the vent and initiates the movement. Also
 * emits the signal indicating the position before the move so that it is
 * easier to track when the actual movement happens (and how long it takes).
 * @param value The new target state
 */
void Vent::setState(double value) {
    qDebug() << "Vent::setState" << value;
    if (value >= 0 && value <= 100 && value != mState) {
        sendPositionSignal(false);
        mState = value;
        mActuator->setTargetPosition(convertStateToPosition(mState));
    }
}

/*
 * The slot called when the linear actuator emits the position update signal
 * @param newPosition The updated position of the linear actuator in millimeters
 */
void Vent::actuatorUpdate(double newPosition) {
    qDebug() << "Vent state updated:" << convertPositionToState(newPosition)
             << "% (" << newPosition << "mm )";

    sendPositionSignal(true);
}

/*
 * Emits the vent state change signal
 * @param movementFinished Indicates if the signal is sent due to the end of the movement
 */
void Vent::sendPositionSignal(bool movementFinished) {
    emit stateChanged(QDateTime::currentDateTime().toString(SIGNAL_TIME_FORMAT),
        convertPositionToState(mActuator->realPosition()), movementFinished);
}

/*
 * Converts the vent state ('X% open') to the actuator position value expressed in millimeters
 * ('Y mm long'). Note that the actuator is installed so that the vent is open
 * when the actuator is in the shortest position.
 * @param ventState Percentage indicating how open the vent is (0-100%)
 * @return Position of the vent (in mm) corresponding to the state
 */
double Vent::convertStateToPosition(double ventState) const{
    return mActuator->maxLength()*(1.0-ventState/100.0);
}

/*
 * The counterpart of convertStateToPosition() method which makes the conversion to the other direction
 */
double Vent::convertPositionToState(double actuatorPosition) const {
    return 100.0*(1.0-actuatorPosition/mActuator->maxLength());
}
