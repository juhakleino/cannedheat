/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <math.h>
#include <QDebug>
#include <cannedheatsettings.h>
#include "linearactuator.h"

LinearActuator::LinearActuator(QObject *parent) :
        QObject(parent) {
    mTargetPosition = maxLength();
    mStepperMotor = new StepperMotor(convertPositionToSteps(mTargetPosition), this);
    connect(mStepperMotor, SIGNAL(positionChanged(ulong)), this, SLOT(updateRealPosition(ulong)));
    connect(mStepperMotor, SIGNAL(moveFinished(ulong)), this, SLOT(moveCompleted(ulong)));
}

int LinearActuator::initialize() {
    int status = 0;

    status = mStepperMotor->initialize();

    return status;
}

/*
 * The max length to which the linear actuator can be driven
 * @return the max length in millimeters
 */
double LinearActuator::maxLength() const {
    return CannedheatSettings::instance().maxMovement();
}

/*
 * The actual length of the actuator
 * @return The current length of the actuator in millimeters
 */
double LinearActuator::realPosition() const {
    return convertStepsToPosition(mStepperMotor->position());
}

/*
 * The requested length of the actuator. This differs from the real length while
 * the actuator is making the requested move.
 * @return The requested length of the actuator in millimeters
 */
double LinearActuator::targetPosition() const {
    return mTargetPosition;
}

/*
 * Sets the target length of the actuator and starts the movement.
 * @param newValue The requested length in millimeters
 */
void LinearActuator::setTargetPosition(double newValue) {
    if (newValue >= 0
            && newValue <= maxLength()
            && newValue != mTargetPosition) {
        mTargetPosition = newValue;
        mStepperMotor->move(convertPositionToSteps(mTargetPosition)
            - mStepperMotor->position());
    }
}

/*
 * The slot which is called when the stepper motor emits the state change signal
 * @param stepperMotorPosition The position of the stepper motor in 'steps'
 */
void LinearActuator::updateRealPosition(ulong stepperMotorPosition) {
    double newPosition = convertStepsToPosition(stepperMotorPosition);
    qDebug() << "Linear actuator position changed:" << newPosition << "mm ("
        << stepperMotorPosition << "steps )";
}

/*
 * The slot which is called when the stepper motor has finished the requested movement
 * @param stepperMotorPosition The position of the stepper motor in 'steps'
 */
void LinearActuator::moveCompleted(ulong stepperMotorPosition) {
    double newPosition = convertStepsToPosition(stepperMotorPosition);
    emit realPositionChanged(newPosition);
}


/*
 * Converts the actuator position to the stepper motor position
 * @param position The length of the actuator in millimeters
 * @return The number of steps the stepper motor have to take to move the actuator
 *         from the 'point zero' to this position
 */
ulong LinearActuator::convertPositionToSteps(double position) {
    double rotations = position/CannedheatSettings::instance().actuatorScrewPitch();
    ulong steps = round(rotations*CannedheatSettings::instance().stepsPerRotation());
    return steps;
}

/*
 * The counterpart of the convertPositionToSteps() method which converts the stepper motor
 * position to the actuator position
 * @param steps The stepper motor position presented as steps from the 'point zero'
 * @return The length of the actuator in millimeters
 */
double LinearActuator::convertStepsToPosition(ulong steps) {
    double rotations = (double) steps/(double) CannedheatSettings::instance().stepsPerRotation();
    double position = rotations*CannedheatSettings::instance().actuatorScrewPitch();
    return position;
}
