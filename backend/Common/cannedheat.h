/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * cannedheat.h
 *
 *  Created on: Jun 8, 2014
 *      Author: Juha Leino
 */

#ifndef CANNEDHEAT_H_
#define CANNEDHEAT_H_

#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include "configuration.h"

// Types
typedef struct {
    double grill_temperature;
    double meat_temperature;
} ch_temperature_measurements_t;

// DBus related constants
#define DBUS_LOGGER_SERVER_NAME "fi.juhakleino.cannedheat.logger"
#define DBUS_LOGGER_OBJECT_NAME "/fi/juhakleino/cannedheat/logger/object"
#define DBUS_LOGGER_INTERFACE_NAME "fi.juhakleino.cannedheat.logger"
#define DBUS_LOGGER_CLEAR_METHOD_NAME "Clear"
#define DBUS_LOGGER_STATE_PROPERTY_NAME "state"

#define DBUS_MEASURER_SERVER_NAME "fi.juhakleino.cannedheat.measurer"
#define DBUS_MEASURER_OBJECT_NAME "/fi/juhakleino/cannedheat/measurer/object"
#define DBUS_MEASURER_INTERFACE_NAME "fi.juhakleino.cannedheat.measurer"
#define DBUS_MEASURER_TEMPERATURE_SIGNAL_NAME "TemperatureMeasurement"
#define DBUS_MEASUREMENT_SIGNAL_FORMAT "(sdd)"
#define DBUS_MEASUREMENT_TIME_STRING_FORMAT "%F %H:%M:%S"

#define DBUS_VENT_SERVER_NAME "fi.juhakleino.cannedheat.vent"
#define DBUS_VENT_OBJECT_NAME "/fi/juhakleino/cannedheat/vent/object"
#define DBUS_VENT_INTERFACE_NAME "fi.juhakleino.cannedheat.vent"
#define DBUS_VENT_SIGNAL_NAME "stateChanged"
#define DBUS_VENT_SIGNAL_FORMAT "(sdb)" 
#define DBUS_VENT_STATE_PROPERTY_NAME "state"

#define TEMPERATURE_CONTROLLER_APP_NAME "Temperature Controller"
#define TEMPERATURE_CONTROLLER_DBUS_OBJECT_NAME "/fi/juhakleino/cannedheat/temperature_controller/object"
#define TEMPERATURE_CONTROLLER_DBUS_SERVICE_NAME "fi.juhakleino.cannedheat.temperature_controller"
#define TEMPERATURE_CONTROLLER_DBUS_INTERFACE_NAME "fi.juhakleino.cannedheat.temperature_controller"
#define TEMPERATURE_CONTROLLER_ENABLED_PROPERTY_NAME "enabled"
#define TEMPERATURE_CONTROLLER_GRILL_TARGET_TEMPERATURE_PROPERTY_NAME "grillTargetTemperature"
#define TEMPERATURE_CONTROLLER_MEAT_TARGET_TEMPERATURE_PROPERTY_NAME "meatTargetTemperature"
#define TEMPERATURE_CONTROLLER_SIGNAL_NAME "configurationChanged"
#define TEMPERATURE_CONTROLLER_SIGNAL_FORMAT "(sbdd)"

#define SIGNAL_TIME_FORMAT "yyyy-MM-dd hh:mm:ss"

// Constants
static const short STATUS_OK = 0;
static const short STATUS_ERR = -1;
static const int LOG_FACILITY=LOG_DAEMON;
static const double CH_MISSING_VALUE = -100; // Used as a placeholder for missign values

// Macros
#define WRITE_SYSLOG(PRIO, ...) openlog(LOG_DAEMON_NAME, LOG_PID, LOG_FACILITY); \
    syslog(PRIO, __VA_ARGS__); \
    closelog();

#ifdef DEBUG

#define TRACE(...) if (active_configuration()->debug_mode) fprintf(stderr, __VA_ARGS__)

#else

#define TRACE(...)

#endif

#endif /* CANNEDHEAT_H_ */
