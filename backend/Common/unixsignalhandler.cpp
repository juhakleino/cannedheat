/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <unistd.h>
#include <QCoreApplication>
#include <cannedheatsettings.h>

/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <loggingutilities.h>
#include "unixsignalhandler.h"

int UnixSignalHandler::sigusr1Fd[2];
int UnixSignalHandler::sigexitFd[2];

/*
 * The class that handled the Unix signals.
 * Look https://qt-project.org/doc/qt-4.7/unix-signals.html for further information
 */
UnixSignalHandler::UnixSignalHandler(QObject *parent)
        : QObject(parent) {
    if (::socketpair(AF_UNIX, SOCK_STREAM, 0, sigusr1Fd)) {
        LoggingUtilities::FatalError("Couldn't create HUP socketpair");
    }

    if (::socketpair(AF_UNIX, SOCK_STREAM, 0, sigexitFd)) {
       LoggingUtilities::FatalError("Couldn't create TERM socketpair");
    }
    snUsr1 = new QSocketNotifier(sigusr1Fd[1], QSocketNotifier::Read, this);
    connect(snUsr1, SIGNAL(activated(int)), this, SLOT(handleSigUsr1()));
    snExit = new QSocketNotifier(sigexitFd[1], QSocketNotifier::Read, this);
    connect(snExit, SIGNAL(activated(int)), this, SLOT(handleExitSignals()));
}

int UnixSignalHandler::setupUnixSignalHandlers() {
    int status = 0;
    struct sigaction usr1;
    struct sigaction term;

    usr1.sa_handler = UnixSignalHandler::usr1SignalHandler;
    sigemptyset(&usr1.sa_mask);
    usr1.sa_flags = 0;
    usr1.sa_flags |= SA_RESTART;

    if (sigaction(SIGUSR1, &usr1, 0) > 0) {
       status = 1;
    }

    term.sa_handler = UnixSignalHandler::exitSignalHandler;
    sigemptyset(&term.sa_mask);
    term.sa_flags |= SA_RESTART;

    if (sigaction(SIGTERM, &term, 0) > 0) {
       status = 1;
    }

    term.sa_handler = UnixSignalHandler::exitSignalHandler;
    sigemptyset(&term.sa_mask);
    term.sa_flags |= SA_RESTART;

    if (sigaction(SIGINT, &term, 0) > 0) {
       status = 1;
    }

    return status;
}

void UnixSignalHandler::usr1SignalHandler(int unused) {
    Q_UNUSED(unused)
    char a = 1;
    ssize_t written = ::write(sigusr1Fd[0], &a, sizeof(a));

    if (written != sizeof(a)) {
        LoggingUtilities::Syslog(LOG_INFO, "Socket problem in the signal handler");
    }
}

void UnixSignalHandler::exitSignalHandler(int unused) {
    Q_UNUSED(unused)
    char a = 1;
    ssize_t written = ::write(sigexitFd[0], &a, sizeof(a));
    if (written != sizeof(a)) {
        LoggingUtilities::Syslog(LOG_INFO, "Socket problem in the signal handler");
    }
}

void UnixSignalHandler::handleSigUsr1() {
    snUsr1->setEnabled(false);
    char tmp;
    ssize_t bytesRead = ::read(sigusr1Fd[1], &tmp, sizeof(tmp));

    if (bytesRead != sizeof(tmp)) {
        LoggingUtilities::Syslog(LOG_INFO, "Socket problem in the signal handler");
    }

    LoggingUtilities::Syslog(LOG_INFO, "USR1 signal caught");
    CannedheatSettings::instance().sync();
    emit reconfigurationRequested();

    snUsr1->setEnabled(true);
}

void UnixSignalHandler::handleExitSignals() {
    snExit->setEnabled(false);
    char tmp;
    ssize_t bytesRead = ::read(sigexitFd[1], &tmp, sizeof(tmp));

    if (bytesRead != sizeof(tmp)) {
        LoggingUtilities::Syslog(LOG_INFO, "Socket problem in the signal handler");
    }

    LoggingUtilities::Syslog(LOG_INFO, "Terminating signal (INT or TERM) caught");
    QCoreApplication::exit();

    snExit->setEnabled(true);
}
