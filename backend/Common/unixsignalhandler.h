/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef VENTCONTROLLERSIGNALHANDLER_H
#define VENTCONTROLLERSIGNALHANDLER_H

#include <QObject>
#include <QSocketNotifier>

/*!
 * \brief The class that handled the Unix signals.
 * Look https://qt-project.org/doc/qt-4.7/unix-signals.html for further information
 */
class UnixSignalHandler : public QObject
{
    Q_OBJECT
public:
    explicit UnixSignalHandler(QObject *parent = 0);

public:
    static int setupUnixSignalHandlers();

signals:
    void reconfigurationRequested();

private slots:
    void handleSigUsr1();
    void handleExitSignals();

private:
    static void usr1SignalHandler(int unused);
    static void exitSignalHandler(int unused);

private:
    static int sigusr1Fd[2];
    static int sigexitFd[2];
    QSocketNotifier *snUsr1;
    QSocketNotifier *snExit;
};

#endif // VENTCONTROLLERSIGNALHANDLER_H
