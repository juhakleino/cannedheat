/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * configuration.c
 *
 *  Created on: Jun 8, 2014
 *      Author: Juha Leino
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "cannedheat.h"
#include "configuration.h"

// Constants
char const * const SERVER_CONFIGURATION_FILE_PATH = "/etc/cannedheat/measurer.conf";
char const * const SAMPLING_FREQUENCY_PARAMETER = "samplingFrequency";
char const * const SAMPLE_SIZE_PARAMETER = "sampleSize";
char const * const GRILL_PROBE_PORT_PARAMETER = "grillProbePort";
char const * const MEAT_PROBE_PORT_PARAMETER = "meatProbePort";
char const * const STEINHART_A_PARAMETER = "steinhartA";
char const * const STEINHART_B_PARAMETER = "steinhartB";
char const * const STEINHART_C_PARAMETER = "steinhartC";
char const * const DEBUG_PARAMETER = "debug";

static const uint DEFAULT_SAMPLING_FREQUENCY = 1000; // in ms
static const uint DEFAULT_SAMPLE_SIZE = 10;
static const int DEFAULT_GRILL_PROBE_PORT = 5;
static const int DEFAULT_MEAT_PROBE_PORT = 3;
static const double MAVERICK_ET73_A = 2.4723753e-04;
static const double MAVERICK_ET73_B = 2.3402251e-04;
static const double MAVERICK_ET73_C = 1.3879768e-07;

// Configuration data container
static configuration_parameters_t server_configuration;

/*
 * Prints the command line help text
 */
void print_help() {
    fprintf(stderr, "Accepted configuration parameters:\n");
    fprintf(stderr, "\t%s=time in ms (integer)\n", SAMPLING_FREQUENCY_PARAMETER);
    fprintf(stderr, "\t%s=number of samples per a measurement", SAMPLE_SIZE_PARAMETER);
    fprintf(stderr, "\t%s=analog input port number (0..6)\n", GRILL_PROBE_PORT_PARAMETER);
    fprintf(stderr, "\t%s=analog input port number (0..6)\n", MEAT_PROBE_PORT_PARAMETER);
    fprintf(stderr, "\t%s=Constant A of the temperature sensors in Steinhart-Hart\n", STEINHART_A_PARAMETER);
    fprintf(stderr, "\tequation (floating point number)\n");
    fprintf(stderr, "\t%s=Constant B\n", STEINHART_B_PARAMETER);
    fprintf(stderr, "\t%s=Constant C\n", STEINHART_C_PARAMETER);
    fprintf(stderr, "\t%s\n", DEBUG_PARAMETER);
}

/*
 * Parses a single configuration parameter and stores the information
 * @return STATUS_OK or STATUS_ERR
 */
int parse_configuration_parameter(const char *arg) {
    int status = 0;
    int skippable = 1;
    const char *string_ptr = NULL;
    char *end_ptr = NULL;
    int parameter_name_length = 0;
    int integer_parameter = 0;
    double double_parameter;

    // Check if the line is empty or starts with '#'
    string_ptr = arg;
    while (*string_ptr != '\0') {
        if (!isspace(*string_ptr)) {
            if (*string_ptr != '#') {
                skippable = 0;
            }
            break;
        }
        string_ptr++;
    }

    if (skippable) {
        return status; // Skip the rest of the function!!
    }

    // Check that there is at least one '=' character in the parameter
    string_ptr = strchr(arg, '=');

    if (string_ptr == NULL) {
        if (strcmp(DEBUG_PARAMETER, arg) == 0) {
            server_configuration.debug_mode = 1;
        } else {
            status = STATUS_ERR;
        }
    } else {
        parameter_name_length = string_ptr - arg;

        if (strncmp(SAMPLING_FREQUENCY_PARAMETER, arg, parameter_name_length) == 0
            || strncmp(SAMPLE_SIZE_PARAMETER, arg, parameter_name_length) == 0
            || strncmp(GRILL_PROBE_PORT_PARAMETER, arg, parameter_name_length) == 0
            || strncmp(MEAT_PROBE_PORT_PARAMETER, arg, parameter_name_length) == 0) {
            integer_parameter = (int) strtol(string_ptr + 1, &end_ptr, 10);

            if (end_ptr != string_ptr + 1 && end_ptr && *end_ptr == '\0') {
                if (strncmp(SAMPLING_FREQUENCY_PARAMETER, arg, parameter_name_length) == 0
                    && integer_parameter > 0) {
                    server_configuration.sampling_frequency = (uint) integer_parameter;
                } else if (strncmp(SAMPLE_SIZE_PARAMETER, arg, parameter_name_length) == 0
                    && integer_parameter > 0){
                    server_configuration.sample_size = (uint) integer_parameter;
                } else if (strncmp(GRILL_PROBE_PORT_PARAMETER, arg, parameter_name_length) == 0
                    && integer_parameter >= 0 && integer_parameter <= 6) {
                    server_configuration.grill_probe_port = (uint) integer_parameter;
                } else if (strncmp(MEAT_PROBE_PORT_PARAMETER, arg, parameter_name_length) == 0
                    && integer_parameter >= 0 && integer_parameter <= 6) {
                    server_configuration.meat_probe_port = (uint) integer_parameter;
                } else {
                    status = STATUS_ERR; // Invalid parameter value
                }
            } else {
                status = STATUS_ERR; // Invalid integer value
            }
        } else if (strncmp(STEINHART_A_PARAMETER, arg, parameter_name_length) == 0
                || strncmp(STEINHART_B_PARAMETER, arg, parameter_name_length) == 0
                || strncmp(STEINHART_C_PARAMETER, arg, parameter_name_length) == 0) {
            double_parameter = strtod(string_ptr + 1, &end_ptr);
            if (end_ptr != string_ptr + 1 && end_ptr && *end_ptr == '\0') {
                if (strncmp(STEINHART_A_PARAMETER, arg, parameter_name_length) == 0) {
                    server_configuration.steinhart_a = double_parameter;
                } else if (strncmp(STEINHART_B_PARAMETER, arg, parameter_name_length) == 0) {
                    server_configuration.steinhart_b = double_parameter;
                } else if (strncmp(STEINHART_C_PARAMETER, arg, parameter_name_length) == 0) {
                    server_configuration.steinhart_c = double_parameter;
                }
            } else {
                status = STATUS_ERR; // Invalid floating point value
            }
        } else  {
            status = STATUS_ERR; // Unknown parameter
        }
    }

    if (status != STATUS_OK) {
        fprintf(stderr, "Invalid parameter: %s\n", arg);
    }

    return status;
}

/*
 * Parses the command line parameters.
 * @param argc Like in main()
 * @param argv Like in main()
 * @return STATUS_OK or STATUS_ERR
 */
int parse_command_line(int argc, char const *argv[]) {
    int result = STATUS_OK;
    int i = 1;  // Skipping the process name

    while (i < argc && result == STATUS_OK) {
        if (parse_configuration_parameter(argv[i]) != STATUS_OK) {
            result = STATUS_ERR;
        }
        i++;
    }

    return result;
}

/*
 * Reads the configuration from the file
 * @return STATUS_OK or STATUS_ERR
 */
int read_server_configuration_file() {
    int status = STATUS_OK;
    FILE *configuration_file = NULL;
    char *line = NULL;
    size_t bytes_allocated = 80;
    ssize_t bytes_read = 0;

    line = malloc(bytes_allocated + 1);
    memset(line, 0, bytes_allocated + 1);

    configuration_file = fopen(SERVER_CONFIGURATION_FILE_PATH, "r");

    // Missing file is not interpreted as an error
    if (NULL != configuration_file) {
        while (STATUS_OK == status
            && (bytes_read = getline(&line, &bytes_allocated, configuration_file)) != -1) {
            line[bytes_read-1] = '\0';
            if (parse_configuration_parameter(line) != 0) {
                status = STATUS_ERR;
            }
        }
        fclose(configuration_file);
    }

    free(line);

    return status;
}

/*
 * Reads the command line options and the configuration file and stores the results
 * the global variable
 * @param argc Like in main()
 * @param argv Like in main()
 * @return STATUS_OK or STATUS_ERR
 */
int initialize_configuration(int argc, char const *argv[]) {
    int status = STATUS_OK;

    // Set the default configuration
    server_configuration.sampling_frequency = DEFAULT_SAMPLING_FREQUENCY;
    server_configuration.sample_size = DEFAULT_SAMPLE_SIZE;
    server_configuration.grill_probe_port = DEFAULT_GRILL_PROBE_PORT;
    server_configuration.meat_probe_port = DEFAULT_MEAT_PROBE_PORT;
    server_configuration.steinhart_a = MAVERICK_ET73_A;
    server_configuration.steinhart_b = MAVERICK_ET73_B;
    server_configuration.steinhart_c = MAVERICK_ET73_C;
    server_configuration.debug_mode = 0;
    server_configuration.measurer_state = MEASURER_STATE_PUBLISH_NOSTORE;

    status = read_server_configuration_file();

    // Parameters from the command line write over the configuration file values
    if (STATUS_OK == status) {
        status = parse_command_line(argc, argv);
    }

    if (STATUS_OK == status) {
        printf("\nActive configuration:\n");
        printf("\tSampling frequency: %u\n", server_configuration.sampling_frequency);
        printf("\tSample size: %u\n", server_configuration.sample_size);
        printf("\tGrill probe port: %u\n", server_configuration.grill_probe_port);
        printf("\tMeat probe port: %u\n", server_configuration.meat_probe_port);
        printf("\tSteinhart-Hart Constant A: %g\n", server_configuration.steinhart_a);
        printf("\tSteinhart-Hart Constant B: %g\n", server_configuration.steinhart_b);
        printf("\tSteinhart-Hart Constant C: %g\n", server_configuration.steinhart_c);
    } else {
        print_help();
    }

    return status;
}

/*
 * Returns a (const) pointer to the current configuration data
 * @return a pointer to the configuration data struct
 */
const configuration_parameters_t *active_configuration() {
    return &server_configuration;
}

void set_measurer_state(int new_state) {
    if (MEASURER_STATE_PUBLISH_NOSTORE == new_state
        || MEASURER_STATE_PUBLISH_STORE == new_state) {
        server_configuration.measurer_state = new_state;
    }
}


