/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef CONTROLLERSETTINGS_H
#define CONTROLLERSETTINGS_H

#include <QObject>
#include <QSettings>

/*!
 * \brief The settings data service for Canned Heat processes built on the top of Qt
 */
class CannedheatSettings : public QObject
{
    Q_OBJECT
public:
    static CannedheatSettings &instance();
    static void free();

public:
    // Target temperatures
    Q_PROPERTY(double grillTargetTemperature READ grillTargetTemperature WRITE setGrillTargetTemperature)
    Q_PROPERTY(double meatTargetTemperature READ meatTargetTemperature WRITE setMeatTargetTemperature)

    // PID controller parameters
    Q_PROPERTY(double kc READ kc)
    Q_PROPERTY(double ki READ ki)
    Q_PROPERTY(double kd READ kd)
    Q_PROPERTY(double deadband READ deadband)

    // Vent related parameters
    Q_PROPERTY(double maxMovement READ maxMovement)
    Q_PROPERTY(double actuatorScrewPitch READ actuatorScrewPitch)
    Q_PROPERTY(uint stepsPerRotation READ stepsPerRotation)
    Q_PROPERTY(uint stepperMotorRpm READ stepperMotorRpm)
    Q_PROPERTY(uint stepGpioPin READ stepGpioPin)
    Q_PROPERTY(uint sleepGpioPin READ sleepGpioPin)
    Q_PROPERTY(uint directionGpioPin READ directionGpioPin)
    Q_PROPERTY(bool simulationModeOn READ simulationModeOn WRITE setSimulationModeOn)

public:
    double grillTargetTemperature() const;
    double meatTargetTemperature() const;
    double kc() const;
    double ki() const;
    double kd() const;
    double deadband() const;

    double maxMovement() const;
    double actuatorScrewPitch() const;
    uint stepsPerRotation() const;
    uint stepperMotorRpm() const;
    uint stepGpioPin() const;
    uint sleepGpioPin() const;
    uint directionGpioPin() const;

    bool simulationModeOn() const;

public slots:
    void sync();
    void setGrillTargetTemperature(double newValue);
    void setMeatTargetTemperature(double newValue);
    void setSimulationModeOn(bool newValue);

private:
    explicit CannedheatSettings(QObject *parent = 0);

private:
    static CannedheatSettings *singletonInstance;
    static bool simulationMode;

private:
    QSettings *mSettings;
};

#endif // CONTROLLERSETTINGS_H
