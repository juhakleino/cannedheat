/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <QDebug>
#include "loggingutilities.h"
#include "cannedheatsettings.h"

// Constants
static const double MAX_MOVEMENT_DEFAULT = 75.0; // mm
static const double ACTUATOR_SCREW_PITCH_DEFAULT = 0.7; // mm, M4 screw
static const uint STEPS_PER_ROTATION_DEFAULT = 200;
static const uint STEPPER_MOTOR_RPM_DEFAULT = 60;
static const uint STEP_GPIO_PIN_DEFAULT = 46;
static const uint SLEEP_GPIO_PIN_DEFAULT = 45;
static const uint DIRECTION_GPIO_PIN_DEFAULT = 27;
static const double GRILL_TARGET_TEMPERATURE_DEFAULT = 125.0;
static const double MEAT_TARGET_TEMPERATURE_DEFAULT = 82.0;
static const double CONTROLLER_KC_DEFAULT = 0.0;
static const double CONTROLLER_KI_DEFAULT = 0.0;
static const double CONTROLLER_KD_DEFAULT = 0.0;
static const double CONTROLLER_DEADBAND_DEFAULT = 0.0;

#define MAX_MOVEMENT_SETTING_ID "actuator/max-length"
#define ACTUATOR_SCREW_PITCH_SETTING_ID "actuator/pitch"
#define STEPS_PER_ROTATION_SETTING_ID "stepper/steps-per-rotation"
#define STEPPER_MOTOR_RPM_SETTING_ID "stepper/rpm"
#define STEP_GPIO_PIN_SETTING_ID "beaglebone/step-pin"
#define SLEEP_GPIO_PIN_SETTING_ID "beaglebone/sleep-pin"
#define DIRECTION_GPIO_PIN_SETTING_ID "beaglebone/direction-pin"
#define GRILL_TARGET_TEMPERATURE_SETTING_ID "temperature/grill"
#define MEAT_TARGET_TEMPERATURE_SETTING_ID "temperature/meat"
#define CONTROLLER_KC_SETTING_ID "controller/kc"
#define CONTROLLER_KI_SETTING_ID "controller/ki"
#define CONTROLLER_KD_SETTING_ID "controller/kd"
#define CONTROLLER_DEADBAND_SETTING_ID "controller/deadband"

#define CH_CONFIGURATION_FILE_NAME "/etc/cannedheat/cannedheat.ini"

CannedheatSettings *CannedheatSettings::singletonInstance = NULL;
bool CannedheatSettings::simulationMode = false;

CannedheatSettings::CannedheatSettings(QObject *parent)
    : QObject(parent) {
    mSettings = new QSettings(CH_CONFIGURATION_FILE_NAME, QSettings::IniFormat);
    if (mSettings->status() != QSettings::NoError) {
        if (mSettings->status() == QSettings::FormatError) {
            LoggingUtilities::instance().FatalError("Format error in Cannedheat settings");
        } else {
            LoggingUtilities::instance().FatalError("Cannot access Cannedheat settings");
        }
    }
}

CannedheatSettings &CannedheatSettings::instance() {
    if (!singletonInstance) {
        singletonInstance = new CannedheatSettings();
    }
    return *singletonInstance;
}

void CannedheatSettings::free() {
    delete singletonInstance;
    singletonInstance = NULL;
}

double CannedheatSettings::grillTargetTemperature() const {
    return mSettings->value(GRILL_TARGET_TEMPERATURE_SETTING_ID, GRILL_TARGET_TEMPERATURE_DEFAULT).toDouble();
}

double CannedheatSettings::meatTargetTemperature() const {
    return mSettings->value(MEAT_TARGET_TEMPERATURE_SETTING_ID, MEAT_TARGET_TEMPERATURE_DEFAULT).toDouble();
}

double CannedheatSettings::kc() const {
    return mSettings->value(CONTROLLER_KC_SETTING_ID, CONTROLLER_KC_DEFAULT).toDouble();
}

double CannedheatSettings::ki() const {
    return mSettings->value(CONTROLLER_KI_SETTING_ID, CONTROLLER_KI_DEFAULT).toDouble();
}

double CannedheatSettings::kd() const {
    return mSettings->value(CONTROLLER_KD_SETTING_ID, CONTROLLER_KD_DEFAULT).toDouble();
}

double CannedheatSettings::deadband() const {
    return mSettings->value(CONTROLLER_DEADBAND_SETTING_ID, CONTROLLER_DEADBAND_DEFAULT).toDouble();
}

void CannedheatSettings::setGrillTargetTemperature(double newValue) {
    mSettings->setValue(GRILL_TARGET_TEMPERATURE_SETTING_ID, newValue);
}

void CannedheatSettings::setMeatTargetTemperature(double newValue) {
    qDebug() << "CannedheatSettings::setMeatTargetTemperature" << newValue;
    mSettings->setValue(MEAT_TARGET_TEMPERATURE_SETTING_ID, newValue);
}

double CannedheatSettings::maxMovement() const {
    return mSettings->value(MAX_MOVEMENT_SETTING_ID, MAX_MOVEMENT_DEFAULT).toDouble();
}

double CannedheatSettings::actuatorScrewPitch() const {
    return mSettings->value(ACTUATOR_SCREW_PITCH_SETTING_ID, ACTUATOR_SCREW_PITCH_DEFAULT).toDouble();
}

uint CannedheatSettings::stepsPerRotation() const {
    return mSettings->value(STEPS_PER_ROTATION_SETTING_ID, STEPS_PER_ROTATION_DEFAULT).toUInt();
}

uint CannedheatSettings::stepperMotorRpm() const {
    return mSettings->value(STEPPER_MOTOR_RPM_SETTING_ID, STEPPER_MOTOR_RPM_DEFAULT).toUInt();
}

uint CannedheatSettings::stepGpioPin() const {
    return mSettings->value(STEP_GPIO_PIN_SETTING_ID, STEP_GPIO_PIN_DEFAULT).toUInt();
}

uint CannedheatSettings::sleepGpioPin() const {
    return mSettings->value(SLEEP_GPIO_PIN_SETTING_ID, SLEEP_GPIO_PIN_DEFAULT).toUInt();
}

uint CannedheatSettings::directionGpioPin() const {
    return mSettings->value(DIRECTION_GPIO_PIN_SETTING_ID, DIRECTION_GPIO_PIN_DEFAULT).toUInt();
}

bool CannedheatSettings::simulationModeOn() const {
    return CannedheatSettings::simulationMode;
}

void CannedheatSettings::setSimulationModeOn(bool newValue) {
    CannedheatSettings::simulationMode = newValue;
}

void CannedheatSettings::sync() {
    mSettings->sync();
}

