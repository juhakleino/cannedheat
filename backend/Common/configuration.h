/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * configuration.h
 *
 *  Created on: Jun 8, 2014
 *      Author: Juha Leino
 */

#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

// Constants
static const int MEASURER_STATE_PUBLISH_NOSTORE = 1;
static const int MEASURER_STATE_PUBLISH_STORE   = 3;

typedef struct {
    uint sampling_frequency;
    uint sample_size;
    uint grill_probe_port;
    uint meat_probe_port;
    double steinhart_a;
    double steinhart_b;
    double steinhart_c;
    uint debug_mode;

    int measurer_state;
} configuration_parameters_t;

int initialize_configuration(int argc, char const *argv[]);
int read_server_configuration_file();
const configuration_parameters_t *active_configuration();
void set_measurer_state(int new_state);

#endif /* CONFIGURATION_H_ */
