/*
 * Copyright (C) 2014 Juha Leino
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <QDebug>
#include <QCoreApplication>
#include <QTime>
#include "loggingutilities.h"

LoggingUtilities *LoggingUtilities::singleton = NULL;
char *LoggingUtilities::appName = NULL;

LoggingUtilities::LoggingUtilities()
    : QObject(NULL) {
}

/*!
 * \brief Sets the application name to be shown in the logs
 * \param appName The application name
 */
void LoggingUtilities::setAppName(QString appName) {
    QByteArray tmp = appName.toLocal8Bit();
    LoggingUtilities::appName = new char[tmp.length()+1]();
    strcpy(LoggingUtilities::appName, tmp.data());

    LoggingUtilities::instance(); // to open the log
}

/*
 * @returns the singleton instance of the class
 */
LoggingUtilities &LoggingUtilities::instance() {
    if (!singleton) {
        singleton = new LoggingUtilities();
        openlog(LoggingUtilities::appName, LOG_PID, LOG_DAEMON);
    }
    return *singleton;
}

/*
 * Frees the resources allocated for the class and the singleton instance
 */
void LoggingUtilities::free() {
    closelog();
    delete LoggingUtilities::singleton;
    LoggingUtilities::singleton = NULL;
    delete LoggingUtilities::appName;
    LoggingUtilities::appName = NULL;
}

/*
 * Writes the error message to the syslog and requests the application to exit
 * @param message The message to write to the syslog
 */
void LoggingUtilities::FatalError(QString message) {
    Syslog(LOG_ERR, message);
    QCoreApplication::exit(1);
}

/*
 * Writes the message to the syslog
 * @param level The priority of the log item
 * @param message The message to write to the log
 */
void LoggingUtilities::Syslog(int level, QString message) {
    QTime rightNow = QTime::currentTime();
    qDebug() << rightNow.toString() + " " + QString::number(level) + " " + message;
    syslog(level, (const char *) message.toLatin1(), "");
}
