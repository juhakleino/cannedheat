# README #

### What is this repository for? ###

The repository is set up for publishing the results of the exercise project that is made as part of the embedded systems course that is arranged in Tampere, Finland in May-Oct 2014. As this is a training project, for time being, I'm not planning to accept any contributions or to make complete instructions for reproducing my test configuration.

The project implements the automatic temperature controller for a kettle grill using Beaglebone Black and Linux as the development platform. The introduction and further documentation is available in [the wiki pages of the project](https://bitbucket.org/juhakleino/cannedheat/wiki).

Repository owner: Juha Leino (fi.linkedin.com/in/juhakleino/)